#python
#cython

To manually compile a file use the cython command in the normal (not python) terminam

cython myfile.pyx

This creates a C file in the same directory.

common arguments
----------------

Simply type cyton in terminal to see all arguments

--cplus		compile as C++
-a		generate an annotated HTML file
-2 or -3	main Python versions used

