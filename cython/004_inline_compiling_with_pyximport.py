#python
#cython
#pyximport


# When using the following code a pyx module is imported and compiled in-line
# This can make development simpler, but requires cython and a C compiler installed 
# for anyone using the code. The module is recompiled only if the code changes.



import pyximport
pyximport.install()

import fib # this is a pyx module (normal python code saved with pyx extension)
# a .pyxdeps file can also be imported which lists other dependencies such as other C files
# a .pyxbld file can be used to list multiple pyx modules to build into one ocmpilation 
 
