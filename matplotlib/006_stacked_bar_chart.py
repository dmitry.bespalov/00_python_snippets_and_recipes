#python
#matplotlib

import matplotlib.pyplot as plt
import numpy as np

a=[5.,30.,45.,22.]
b=[5.,25.,50.,20.]
x=range(4)

plt.bar(x,a,color='b')
plt.bar(x,b,color='r',bottom=a) # bottom specifies the y starting position for a bar

plt.show()
