#python
#matplotlib
#latex

# Requires a LaTeX interpreter to be installed
# Use $ at start and end of string to indicated to MatPlotLib to use LaTeX
# Latex uses \ a lot. By itself this is an escape character in Python, so needs to be provided as \\
# \\ can be avoided by prefixing string with r ... r'$f(a)=\frac....

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-4,4,1024)
y=0.25*(x+4)*(x+1)*(x-2)

plt.title('$f(x)=\\frac{1}{4}(x+4)(x+1)(x-2)$')
plt.plot(x,y,c='k')

plt.show()
