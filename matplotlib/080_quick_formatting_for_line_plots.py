#python
#matplotlib
#line plot

import matplotlib.pyplot as plt

x=[1,2,3,4,5,6]
y=[2,4,6,7,8,8.5]

plt.plot(x,y,'ro-.') # color, symbol, line
plt.show()



# colors: r (red) ,g (green),b (blue),c (cyan) ,m (magenta),y (yellow),k (black),w (white)

# shape: o ^ V < > s * p H h + x d D | -

# line: -, --, -., :

