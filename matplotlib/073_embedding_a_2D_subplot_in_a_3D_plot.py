#python
#matplotlib
#3d
#subplot

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cm

x=np.linspace(-3,3,256)
y=np.linspace(-3,3,256)
X,Y=np.meshgrid(x,y)
Z=np.exp(-(X**2 + Y**2))
u=np.exp(-(x**2))

fig=plt.figure()
ax=fig.gca(projection='3d')
ax.set_zlim3d(0,3)
# Plot the 2D lines
# ax.plot is a 2d rendering
# zdir determines which plane to plot the line in
# zs determines the offset of the plane
ax.plot(x,u,zs=3,zdir='y',lw=2,color='0.75') 
ax.plot(x,u,zs=-3,zdir='x',lw=2,color='k')

# Plot the surface
surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,linewidth=0, antialiased=False)

# Add a colorbar
fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()

