#python
#matplotlib
#scatter

import numpy as np
import matplotlib.pyplot as plt

a=np.random.standard_normal((100,2))
a+=np.array((-1,-1))
b=np.random.standard_normal((100,2))
b+=np.array((1,1))

plt.scatter(a[:,0],a[:,1],color='1.00',edgecolor='k',marker='^')
plt.scatter(b[:,0],b[:,1],color='k',marker='x')       

plt.show()



# Unlike color, marker type cannot be read from a list. 
# To display diifferent markers for different categories in a list they need to be separated:
# This plot uses iris data

import numpy as np
import matplotlib.pyplot as plt

label_set=(
        b'Iris-setosa',
        b'Iris-versicolor',
        b'Iris-virginica',
        )

def read_label(label):
    return (label_set.index(label))

# Iris data is loaded and name is converted to value
data=np.loadtxt('data/iris.csv',delimiter=',',converters={4:read_label})

marker_set=('^','x','.')

for i,marker in enumerate(marker_set):
        
     # data_subset=np.asarray([x for x in data if x[4]==i]) # book code
     data_subset=data[data[:,4]==i] # faster way

    plt.scatter(data_subset[:,0],data_subset[:,1],color='k',marker=marker)

plt.show()

