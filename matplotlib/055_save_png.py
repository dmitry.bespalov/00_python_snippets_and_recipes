#python
#matplotlib
#png

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-10,10,1024)
y=np.sinc(x)

plt.plot(x,y)

plt.savefig('sinc.png',c='k')

Generates a png file ....
Default size is 600 x 600 with 8 bit colours
Default dpi = 100

Options on savefig:
transparent='True'  (use alpha=x in the plt function)
dpi=300 # file output of standard plot will be 2400 x 1800 pixels (changing dpi changes the apparent size of annotations, if size/aspect also changed in figure)

