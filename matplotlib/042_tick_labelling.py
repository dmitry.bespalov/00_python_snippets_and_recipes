#python
#matplotlib
#tick

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

# ticker.Locator generateslocation of ticks
# ticker.Formatter generates labels for ticks
# FixedFormatter used below takes labels from a list

name_list=('Omar','Serguey','Max','Zhou','Abidin')
value_list=np.random.randint(10,99,size=len(name_list))
pos_list=np.arange(len(name_list))

ax=plt.axes()
ax.xaxis.set_major_locator(ticker.FixedLocator((pos_list)))  
ax.xaxis.set_major_formatter(ticker.FixedFormatter((name_list)))

plt.bar(pos_list,value_list,color='0.75',align='center')

plt.show()

# ===========================================================
# A simpler way to generate the same without using ticker....
# ===========================================================

import numpy as np
import matplotlib.pyplot as plt

name_list=('Omar','Serguey','Max','Zhou','Abidin')
value_list=np.random.randint(10,99,size=len(name_list))
pos_list=np.arange(len(name_list))

plt.bar(pos_list,value_list,color='0.75',align='center')
plt.xticks(pos_list,name_list)

plt.show()
