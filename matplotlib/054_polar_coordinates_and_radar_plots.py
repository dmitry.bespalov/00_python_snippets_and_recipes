#python
#matplotlib
#polar
#radar


import matplotlib.pyplot as plt
import numpy as np

t=np.linspace(0,2*np.pi,1024)

plt.axes(polar=True)
plt.plot(t,1+0.25*np.sin(16*t), c='k')

plt.show()


# =====================
# Filling in radar plot
# =====================


import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np

ax=plt.axes(polar=True)

theta=np.linspace(0,2*np.pi,8,endpoint=False)
radius=0.25+0.75*np.random.random(size=len(theta))
points=np.vstack((theta,radius)).transpose() # points are angles + radius

plt.gca().add_patch(patches.Polygon(points,color='0.75'))

plt.show()
