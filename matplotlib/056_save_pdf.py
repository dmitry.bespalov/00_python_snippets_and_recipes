#python
#matplotlib
#pdf

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-10,10,1024)
y=np.sinc(x)

plt.plot(x,y)


plt.savefig('sinc.pdf')
