#python
#matplotlib
#box plot

import matplotlib.pyplot as plt
import numpy as np

x=np.random.randn(1000) # samples from a normal distribution

plt.boxplot(x)

plt.show()

# ========================================================
# Boxplot can handle multiple boxplots in one numpy array:
# ========================================================

import matplotlib.pyplot as plt
import numpy as np

x=np.random.randn(1000,5) # samples from a normal distribution

plt.boxplot(x)

plt.show()

import matplotlib.pyplot as plt
import numpy as np

x=np.random.randn(1000,5) # samples from a normal distribution

plt.boxplot(x)

plt.show()

# ===============================
# Or a list of lists may be used:
# ===============================

import matplotlib.pyplot as plt
import numpy as np

x1=list(np.random.randn(100)*5) # samples from a normal distribution
x2=list((np.random.randn(50)*2)+5)
x3=list(np.random.randn(250)*3)
x4=list(np.random.randn(70)*10)

x=[x1,x2,x3,x4]

plt.boxplot(x)

plt.show()
