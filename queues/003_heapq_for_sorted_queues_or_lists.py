import heapq

my_list = [1, 31, 25, 75, 90, 21, 43, 69, 85, 0]

# Show smallest and largest n numbers in heapified list
print(heapq.nsmallest(3, my_list))
print(heapq.nlargest(3, my_list))

# Or to heapify list first
heapq.heapify(my_list)

# From a heapify list, access first or last
print (my_list[0])
print(my_list[-1])

# Add an item into a heap
heapq.heappush(my_list, 26)

# Heappush will always push so that pop will remove the lowerst item, but list
# may appear out of order. If ordered list is important then it needs to be
# sorted
my_list.sort()

#Pop from lowest end
print (heapq.heappop(my_list))
print (heapq.heappop(my_list))

# Merge with other heapified list into generator
my_second_list = [10, 4, 74, 19, 10, 56, 88, 92, 85 ] 
heapq.heapify(my_second_list)
merged_heap = heapq.merge(my_list, my_second_list)

for item in merged_heap:
    print (item, end = ' ')

# heapq can take tuples with first value as priority
# The tuple may contain more values, each of which become sorted. For example,
# a second value may contain the order that items are entered so that if two
# items share a priority then the item entered first will be popped first

print ()
h = []
heapq.heappush(h, (4, 'Publish code'))
heapq.heappush(h, (1, 'Write spec'))
heapq.heappush(h, (3, 'Debug Code'))
heapq.heappush(h, (2, 'Write code'))

while len(h) > 0:
    print (heapq.heappop(h))