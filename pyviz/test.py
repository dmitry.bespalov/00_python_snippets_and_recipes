import pandas as pd
import holoviews as hv
from holoviews import opts
hv.extension('bokeh','matplotlib')

macro_df = pd.read_csv('http://assets.holoviews.org/macro.csv', '\t')
key_dimensions   = [('year', 'Year'), ('country', 'Country')]
value_dimensions = [('unem', 'Unemployment'), ('capmob', 'Capital Mobility'),
                    ('gdp', 'GDP Growth'), ('trade', 'Trade')]
macro = hv.Table(macro_df, key_dimensions, value_dimensions)

bars = macro.to.bars(['Year', 'Country'], 'Trade', [])
bars.opts(
    opts.Bars(color=hv.Cycle('Category20'), show_legend=False, stacked=True, 
              tools=['hover'], width=600, xrotation=90))