# Skip an iteration of a loop with continue

for i in range (20):
    if i%3 == 0:
        # go straight to next iteration
        continue
    print (i)
    
# End a loop with break
    
for i in range (20):
    if i == 5:
        # end loop
        break
    print (i)