"""Based on https://github.com/gsurma/cartpole"""

import random
import gym
import numpy as np
from collections import deque
from tensorflow import keras

ENV_NAME = "CartPole-v1"

DISPLAY_ON_SCREEN = True

GAMMA = 0.95
LEARNING_RATE = 0.001

MEMORY_SIZE = 100000
BATCH_SIZE = 20

# Exploration rate is probabolity of choosign a random action
EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.001
EXPLORATION_DECAY = 0.995
TARGET_REWARD = 250

class DQNSolver:
    
    """
    Deep Q Network solver. Includes control variables, memory of 
    state/action/reward/end, neural net, and mehtods to act, remember, and 
    update neural net by sampling from memory.
    """

    def __init__(self, observation_space, action_space):
        """Constructor method. Set up memory and neural net."""
        self.exploration_rate = EXPLORATION_MAX
        self.action_space = action_space
        
        # Set up memory for state/action/reward/next_state/done
        self.memory = deque(maxlen=MEMORY_SIZE)

        # Set up TesorFlow model
        self.model = keras.Sequential()
        
        self.model.add(keras.layers.Dense(
                24, input_shape=(observation_space,), activation="relu"))
        
        self.model.add(keras.layers.Dense(
                24, activation="relu"))
        
        self.model.add(keras.layers.Dense(
                self.action_space, activation="linear"))
        
        self.model.compile(
                loss="mse", optimizer=keras.optimizers.Adam(lr=LEARNING_RATE))

    def remember(self, state, action, reward, next_state, done):
        """state/action/reward/next_state/done"""
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        """Act either randomly or by redicting action that gives max Q"""
        # Act randomly if random number < exploration rate
        if np.random.rand() < self.exploration_rate:
            return random.randrange(self.action_space)
        # Otherwise choose best predicted outcome
        q_values = self.model.predict(state) # Q for all actions
        return np.argmax(q_values[0]) # Index for max Q

    def experience_replay(self):
        """Train update TensorFlow model by sampling from memory"""
        # Do not try to train model if memory is less than reqired batch size
        if len(self.memory) < BATCH_SIZE:
            return
        # Reduce exploration rate
        self.exploration_rate *= EXPLORATION_DECAY
        self.exploration_rate = max(EXPLORATION_MIN, self.exploration_rate)
        # Sample a random batch from memory
        batch = random.sample(self.memory, BATCH_SIZE)
        for state, action, reward, state_next, terminal in batch:
            # If terminal then Q = Reward
            q_update = reward
            # Otherwise Q is from Bellman equation
            if not terminal:
                # Get best possible Q for next action
                best_next_q = np.amax(self.model.predict(state_next)[0])
                # Calculate current Q using Bellman equation
                q_update = (reward + GAMMA * best_next_q)
            # Get predicted Q values for current state
            q_values = self.model.predict(state)
            # Update predicted Q for current state/action
            q_values[0][action] = q_update
            # Update TensorFlow neural net
            self.model.fit(state, q_values, verbose=0)


def cartpole():
    """Main program loop"""
    # Set up game environemnt
    env = gym.make(ENV_NAME)
    # Get number of observations returned for state
    observation_space = env.observation_space.shape[0]
    # Get number of actions possible
    action_space = env.action_space.n
    # Set up TensorFlow neural net
    dqn_solver = DQNSolver(observation_space, action_space)
    # Set up run counter and start loop    
    run = 0
    continue_learning = True
    while continue_learning:
        run += 1
        # Start run and get first state observations
        state = env.reset()
        # Reshape state into 2D array with state obsverations as first 'row'
        state = np.reshape(state, [1, observation_space])
        step = 0
        while True:
            step += 1
            # Option rander to screen
            #if DISPLAY_ON_SCREEN:
            #    env.render()
            # Get action to take
            action = dqn_solver.act(state)
            # Act
            state_next, reward, terminal, info = env.step(action)
            if DISPLAY_ON_SCREEN:
                env.render()
            # Convert step reward to negative if end of run
            reward = reward if not terminal else -reward
            # Get observations for new state
            state_next = np.reshape(state_next, [1, observation_space])
            # Record state, action, reward, new state & terminal
            dqn_solver.remember(state, action, reward, state_next, terminal)
            # Update state
            state = state_next
            # Actions to take if end of game episode
            if terminal:
                print ("Run: " + str(run) + ", exploration: " + 
                       str(dqn_solver.exploration_rate) + ", score: " + str(step))
                if step > TARGET_REWARD:
                    continue_learning = False
                break
            # Update neural net 
            dqn_solver.experience_replay()


if __name__ == "__main__":
    cartpole()