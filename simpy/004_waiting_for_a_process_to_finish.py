#python
#simpy

# The following calls a 'charge' method which runs and then returns control to the 'run' method which moves to driving.

import simpy

class Car(object):
    def __init__(self,env):
        self.env=env
        # Start the run process evertime an instance is created
        self.action=env.process(self.run())

    def run(self):
        while True:
            # Parking
            print('Start parking and charging car at %d' % env.now)
            charge_duration = 5
            # We yield the process that process() returns
            # to wait for it to finish.
            yield self.env.process(self.charge(charge_duration))

            # Driving starts after charge process has finished
            print('Start driving car at %d' % env.now)
            trip_duration=20
            yield env.timeout(trip_duration)

    def charge(self,duration):
        yield self.env.timeout(duration)



env = simpy.Environment()
car=Car(env)
env.run(until=40)

Output:
Start parking and charging car at 0
Start driving car at 5
Start parking and charging car at 25
Start driving car at 30



