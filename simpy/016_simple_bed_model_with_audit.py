#python
#simpy

import simpy
import random
import pandas as pd

class g():
    # Global variables
    bed_count=0
    inter_arrival_time=1
    los=10
    sim_duration=100
    audit_time=[]
    audit_beds=[]
    audit_interval=1
    
def new_admission(env,interarrival_time,los):
    i=0
    while True:
        i+=1
        p_los=random.expovariate(1/los)
        p=patient(env,i,p_los)
        env.process(p)
        next_p=random.expovariate(1/interarrival_time)
        # print('Next patient in %f3.2' %next_p)
        yield env.timeout(next_p)
        
def patient(env,i,p_los):
    g.bed_count+=1
    # print('Patient %d arriving %7.2f, bed count %d' %(i,env.now,g.bed_count))
    yield env.timeout(p_los)
    g.bed_count-=1
    # print('Patient %d leaving %7.2f, bed count %d' %(i,env.now,g.bed_count))
    
def audit_beds(env,delay):
    yield env.timeout(delay)
    while True:
        g.audit_time.append(env.now)
        g.audit_beds.append(g.bed_count)
        yield env.timeout(g.audit_interval)
        
def build_audit_report():
    audit_report=pd.DataFrame()
    audit_report['Time']=g.audit_time
    audit_report['Occupied beds']=g.audit_beds
    return audit_report
    
def main():
    env=simpy.Environment()
    env.process(new_admission(env,g.inter_arrival_time,g.los))
    env.process(audit_beds(env,delay=20))
    
    env.run(until=g.sim_duration)
    
    audit_report=build_audit_report()
    print('Average beds: %.1f' %audit_report['Occupied beds'].mean())
    print('90th Percentile: %.1f' %audit_report['Occupied beds'].quantile(0.9))
    
if __name__=='__main__':
    main()