# Bed model with audit and chart

import simpy
import random
import matplotlib.pyplot as plt

class g():
    # Global variables (by using a class these may be accessed and changed
    # from within functions)
    bed_count=0 # records occupied beds
    inter_arrival_time=1 # average days between patient arrivals
    los=10 # average length of stay
    sim_duration=500 # duration of sim (days)
    audit_time=[] # records day for audit
    audit_beds=[] # records beds occupied for audit
    audit_interval=1 # number of days between bed audits

def new_admission(env,interarrival_time,los):
    # This is the new admission process
    while True:
        p_los=random.expovariate(1/los) # set length of stay (exponential dist)
        # Call new process (patient length of stay in hospital)
        env.process(patient(env,p_los))
        # set time of next arrival (exponential distibution)
        next_p=random.expovariate(1/interarrival_time) 
        yield env.timeout(next_p) # delay before next arrival

def patient(env,p_los):
    # This is the process for triggering length of stay in hospital
    g.bed_count+=1 # increment bed count
    yield env.timeout(p_los) # delay before patient leaves
    g.bed_count-=1 # decrement bed count

def audit_beds(env,delay):
    # This is the audit process
    yield env.timeout(delay) # delay before first audit
    while True: # loop continually (stopped by end of simulation run)
        g.audit_time.append(env.now) # add current time to audit_time list
        g.audit_beds.append(g.bed_count) # add occupied beds to audit_beds list
        yield env.timeout(g.audit_interval) # trigger delay to next arrival

def chart():
    # Plot beds occupied over time
    plt.plot(g.audit_time, g.audit_beds) # x and y data
    plt.xlabel('Day')
    plt.ylabel('Occupied beds') 
    plt.title('Occupied beds')
    plt.show()

def main():
    # Initialise environment
    env=simpy.Environment()
    # Initialise starting processes (admissions & bed audit)
    env.process(new_admission(env,g.inter_arrival_time,g.los))
    env.process(audit_beds(env,delay=20))
    # Start simulation run
    env.run(until=g.sim_duration)
    # At end of run call chart
    chart()

# Code entry point: call main function
main()
