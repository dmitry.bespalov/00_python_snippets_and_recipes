#python
#simpy

class Patient:
     def __init__(self,patient_id):
        self.id=patient_id
        self.name='name: Patient '+str(patient_id)

patients={}

for i in range(10):
    x=Patient(i)
    patients[x.id]=x

for key in patients:
    print (key,patients[key].name)
    
remove_id=4
del patients[remove_id]

print ('\nRemove patients 4:')

for key in patients:
    print (key,patients[key].name)
