#python
#simpy


import simpy
import random
import pandas as pd
import matplotlib.pyplot as plt

class global_vars:
    bed_count=0
    queue=0

def new_admission(env,interarrival_time,los):
    i=0
    while True:
        i+=1
        p_los=random.expovariate(1/los)
        p=trigger_patient_arrival(env,i,p_los)
        env.process(p)
        next_p=random.expovariate(1/interarrival_time)
        # print('Next patient in %f3.2' %next_p)
        yield env.timeout(next_p)

def trigger_patient_arrival(env,i,p_los):
    with beds_resource.request() as req:
        global_vars.queue+=1
        yield req # returns when required resource available
        global_vars.queue-=1
        global_vars.bed_count+=1
        # print('Patient %d arriving %7.2f, bed count %d' %(i,env.now,g.bed_count))
        yield env.timeout(p_los)
        global_vars.bed_count-=1
        # print('Patient %d leaving %7.2f, bed count %d' %(i,env.now,g.bed_count))

def perform_bed_audit(env,delay):
    yield env.timeout(delay)
    while True:
        audit_time.append(env.now)
        audit_beds.append(global_vars.bed_count)
        audit_queue.append(global_vars.queue)
        yield env.timeout(audit_interval)

def build_audit_report():
    audit_report=pd.DataFrame()
    audit_report['Time']=audit_time
    audit_report['Occupied_beds']=audit_beds
    audit_report['Median_beds']=audit_report['Occupied_beds'].quantile(0.5)
    audit_report['Beds_5_percent']=audit_report['Occupied_beds'].quantile(0.05)
    audit_report['Beds_95_percent']=audit_report['Occupied_beds'].quantile(0.95)
    audit_report['Queue']=audit_queue
    audit_report['Median_queue']=audit_report['Queue'].quantile(0.5)
    audit_report['Queue_5_percent']=audit_report['Queue'].quantile(0.05)
    audit_report['Queue_95_percent']=audit_report['Queue'].quantile(0.95)
    return audit_report

def chart():
    
    # Plot occupied beds
    plt.plot(audit_dataframe['Time'],audit_dataframe['Occupied_beds'],color='k',marker='o',linestyle='solid',markevery=1,label='Occupied beds')
    plt.plot(audit_dataframe['Time'],audit_dataframe['Beds_5_percent'],color='0.5',linestyle='dashdot',markevery=1,label='5th percentile')
    plt.plot(audit_dataframe['Time'],audit_dataframe['Median_beds'],color='0.5',linestyle='dashed',label='Median')
    plt.plot(audit_dataframe['Time'],audit_dataframe['Beds_95_percent'],color='0.5',linestyle='dashdot',label='95th percentile')
    plt.xlabel('Day')
    plt.ylabel('Occupied beds') 
    plt.title('Occupied beds (individual days with 5th, 50th and 95th percentiles)')
    plt.legend()
    plt.show()
    
    # Plot queue for beds
    plt.plot(audit_dataframe['Time'],audit_dataframe['Queue'],color='k',marker='o',linestyle='solid',markevery=1,label='Occupied beds')
    plt.plot(audit_dataframe['Time'],audit_dataframe['Queue_5_percent'],color='0.5',linestyle='dashdot',markevery=1,label='5th percentile')
    plt.plot(audit_dataframe['Time'],audit_dataframe['Median_queue'],color='0.5',linestyle='dashed',label='Median')
    plt.plot(audit_dataframe['Time'],audit_dataframe['Queue_95_percent'],color='0.5',linestyle='dashdot',label='95th percentile')
    plt.xlabel('Day')
    plt.ylabel('Queue for beds') 
    plt.title('Queue for beds (individual days with 5th, 50th and 95th percentiles)')
    plt.legend()
    plt.show()


# Global variables
bed_count=0
inter_arrival_time=1
los=10
sim_duration=500
audit_time=[]
audit_beds=[]
audit_queue=[]
audit_interval=1

# Initialise environment
env=simpy.Environment()

# Define beds resource
beds_resource = simpy.Resource(env, capacity=11)

# Initialise processes (admissions & bed audit)
env.process(new_admission(env,inter_arrival_time,los))
env.process(perform_bed_audit(env,delay=20))
# Start simulation run
env.run(until=sim_duration)
# Build audit table
audit_dataframe=build_audit_report()
chart()
    
