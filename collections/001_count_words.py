words = ['look', 'into', 'my', 'eyes',
        'look', 'into', 'my', 'eyes',
        'not', 'around', 'the', 'eyes',
        'into', 'the', 'eyes', "you're", 'under']

from collections import Counter

# Count words
word_counts = Counter(words)
print ('Words counts:')
print (word_counts)

# Show most common
top_three = word_counts.most_common(3)
print ('\nTop three:')
print (top_three)

# Manually increment word count
print ('\nIncrement count of "eyes"')
print (word_counts['eyes'])
word_counts['eyes'] += 1
print (word_counts['eyes'])

# Two spearate word count can be combined with +
more_words = ['eyes', 'eyes','eyes']
more_word_counts = Counter(more_words)
all_word_counts = word_counts + more_word_counts
print ('\nCombined word counts')
print (all_word_counts)
