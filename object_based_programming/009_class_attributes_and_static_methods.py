#python
#oop
#static
#attributes

Classes: class attributes and static methods

Class attributes and static methods are associated with the class as a whole rather than an individual instance. For example a class attribute of "total" may count the number of instances of a class, and is adjusted by a static method (class attributes and static methods often go together)




class Pet (object):
    """A virtual pet"""
    total = 0 # A variable defined outside of a method is a class attribute
   
    @staticmethod
    def status():
        print("\nThe total number of pets is %s" % Pet.total)
   

    def __init__(self,name): # this runs when a new instance is created
        print("A new pet has been born!")
        self.name=name
        number_of_legs=0 # default of zero legs
        Pet.total +=1
       
    def __str__(self): # this is what is retruned if a the object is printed
        rep="Critter object\n"
        rep+="Name: "+self.name+"\n"
        return rep

    def count_legs(self):
        print(self.name, "has", self.number_of_legs, "legs.\n")

  

doug = Pet("Douggy")
timmy= Pet ("Timy")
doug.number_of_legs=3
print (doug)
doug.count_legs()
Pet.status()
print (doug.total) # also accesses class attribute
