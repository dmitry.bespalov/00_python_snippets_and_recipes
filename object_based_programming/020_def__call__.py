#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
__init__ is called when object is instantised

__call_I is called when object is called (object must first exist)
"""

class MyClass:
    def __init__(self, name):
        print ('Initialise object')
        self.name = name
        
    def __call__(self):
        print ('Call object')
        self.age = 51
        print (self.age)
        
my_object = MyClass('Michael')
my_object()

