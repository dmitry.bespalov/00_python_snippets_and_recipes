#python
#oop
#private
#setter

(Note: private variables should normally be kept just as that! They should not usually be accesed
and changed from outside)

class Cat(object):
    total=0
    @staticmethod
    def status():
        print("Total cat count is %s" % Cat.total)

    def __init__(self,name="Moggy"):
        Cat.total +=1
        self.__name=name

    def showname(self):
        print("My name is %s" %self.__name)
      
    @property
    def name(self):
        return self.__name

    @name.setter # sets private name after check
    def name(self,new_name):
        if new_name=="":
            print("A name can't be blank")
        else:
            self.__name=new_name
            print("Name change successful")


harry=Cat("Harry")
harry.showname()
print(harry.name)
harry.name="Tim"
harry.name=""