#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
'property' is the most pythonic way to set and get object attributes (rather
than directly accessing them. It allows checks to be performed, or other
maniulation). Here we use decorators to call the aprropriate getter/sett
"""

class Celsius:
    def __init__(self, temperature = 0):
        self._temperature = temperature

    def to_fahrenheit(self):
        return (self._temperature * 1.8) + 32
    
    # Note we have two methods called 'temperature'
    # But property decorators are used to define the getter (@property)
    # and the setter (@temperature.setter)
    
    # Getter decorator
    @property
    def temperature(self):
        print("Getting value")
        return self._temperature
    
    # Setter decorator
    @temperature.setter
    def temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value
        return
    
    #deleter decorator (deletes object attrivute)
    @temperature.deleter
    def temperature(self):
        print("deleter of temperature called")
        del self._temperature
        return

    
temp = Celsius(23)

# Using the property getter
print (temp.temperature)

# Using the property setter

temp.temperature = 45
print (temp.temperature)

# Delete temperature
del temp.temperature

# Now an exception will occur if we try to access temperature
print (temp.temperature)
