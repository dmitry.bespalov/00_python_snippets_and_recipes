#python
#oop
#class
#priority

class Item:
    def __init__(self,name):
        self.name=name
    def __repr__(self):
        return 'Item({!r})'.format(self.name)

# Create tuple with priority, index, item
# Index allows comparison of tuples when priorty is equal

a=(1,0,Item('foo'))
b=(5,1,Item('Bar'))
c=(1,2,Item('grok'))

print('a<b: ',a<b)
print('a<c: ',a<c) # a is higher priority than c (using reverse of priority and index as measure of priority)





