#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
'property' is the most pythonic way to set and get object attributes (rather
than directly accessing them. It allows checks to be performed, or other
maniulation)
"""

class Celsius:
    def __init__(self, temperature = 0):
        self._temperature = temperature

    def to_fahrenheit(self):
        return (self._temperature * 1.8) + 32

    def get_temperature(self):
        print("Getting value")
        return self._temperature

    def set_temperature(self, value):
        if value < -273:
            raise ValueError("Temperature below -273 is not possible")
        print("Setting value")
        self._temperature = value
        return
        
    def delete_temperature(self):
        print("deleter of temperature called")
        del self._temperature
        return

    temperature = property(get_temperature, set_temperature, delete_temperature)
    
temp = Celsius(23)

# Using the property getter
print (temp.temperature)

# Using the property setter

temp.temperature = 45
print (temp.temperature)

# Or raise an exception
try:
    temp.temperature = -300
except:
    pass
        

# Delete temperature
del temp.temperature

# Now an exception will occur if we try to access temperature
print (temp.temperature)
