import sqlite3
import os

# Remove existing file if present
try:
    os.remove('population.db')
except:
    print()    


# Set up new database
con = sqlite3.connect('population.db') # Creates new database
cur = con.cursor() # tracks position in database

# Create table
# Note data types in sqlite are:
# NULL - type not known
# INTEGER - int
# REAL - float
# TEXT - str
# BLOB - binary data (bytes), such as picture or mp3

# Python works by passing sql commands as a string to sqlite

# create table # options such as NIT NULL and PRIMARY KEY are optional
# Primary key can consist of multiple columns
# Constraint which is optional avoids duplicates
cur.execute('''
                CREATE TABLE PopByRegion(Region TEXT NOT NULL, 
                Population INTEGER,
                PRIMARY KEY (Region))''')


# Pass data to table
cur.execute('INSERT INTO PopByRegion VALUES("Eastern Asia", 1362955)')
cur.execute('INSERT INTO PopByRegion VALUES("North America", 661157)')
cur.execute('INSERT INTO PopByRegion VALUES("Central Africa", 330993)')
cur.execute('INSERT INTO PopByRegion VALUES("Southestern Africa",743112)')
cur.execute('INSERT INTO PopByRegion VALUES("Japan",100562)')

# Use NULL for missing data
cur.execute('INSERT INTO PopByRegion VALUES ("Mars", NULL)')

# To disallow NULL values in any filed:
cur.execute('CREATE TABLE Test (Region TEXT NOT NULL, Population INTEGER)')

# Placeholder may also be used (udeful for using loops to add data):
cur.execute('INSERT INTO PopByRegion VALUES(?,?)', ("Newton Abbot",30000))

# Commit changes to database
con.commit() # commit changes

# retrive data
cur.execute('SELECT Region, Population from PopByRegion ORDER BY Region DESC')
# Can use SELECT * FROM to retrieve all columns


# retreive one line at a time 
x = cur.fetchone()

# retrieve all fro current cursor position
y=cur.fetchall()


# Applying a conditionNULL
cur.execute('SELECT Region, Population from PopByRegion WHERE POPULATION >1e5 ORDER BY Region DESC')
# Use = for equal, and != for not equal

# Using AND
cur.execute('SELECT Region, Population from PopByRegion WHERE POPULATION >1e5 AND Region <"L" ORDER BY Region DESC')

# Update data
cur.execute('''UPDATE PopByRegion SET Population = 100600
                   WHERE Region = "Japan"''')

con.commit() # commit changes

# Deleet data
#cur.execute('DELETE FROM PopByRegion WHERE Region < "L"')
#con.commit()


# Linking data
# ============

# Create another table with info in

cur.execute('''CREATE TABLE PopByCountry(
        Region TEXT NOT NULL, 
        Country TEXT NOT NULL, 
        Population INTEGER,
        CONSTRAINT CountryKey PRIMARY KEY (Region, Country))''')

cur.execute('''INSERT INTO PopByCountry VALUES(
        "Eastern Asia", "China", 1285328)''')

# Or to use loop to add:
countries = ([('Eastern Asia', 'DPR Korea', 24056),
              ('Eastern Asia', 'Hong Kong', 8764),
              ('Eastern Asia', 'Mongolia', 3407),
              ('Eastern Asia', 'Korea', 41491),
              ('Eastern Asia', 'Taiwan', 1433),
              ('North America', 'Canada', 40876),
              ('North America', 'Greenland', 43),
              ('North America', 'Mexico', 126875),
              ('North America', 'Bahamas', 368),
              ('North America', 'USA', 493038)])

for c in countries:
    cur.execute('INSERT INTO PopByCountry VALUES (?,?,?)',(c[0],c[1],c[2]))

con.commit()

# Inner join
# Join creates a cross-product off all rows, need to limit using WHERE
cur.execute('''
            SELECT PopByRegion.Region, PopByCountry.Country 
            FROM   PopByRegion INNER JOIN PopByCountry 
            WHERE  (PopByRegion.Region = PopByCountry.Region) 
            AND    (PopByRegion.Population > 1000000)
            ''')

print ('\n',cur.fetchall())
# Can use maths
# Where a query will produce duplicates use DISTINCT:

cur.execute('''
    SELECT DISTINCT PopByRegion.Region
    FROM PopByRegion INNER JOIN PopByCountry
    WHERE (PopByRegion.Region = PopByCountry.Region)
    AND ((PopByCountry.Population * 1.0) / PopByRegion.Population > 0.10)''')


print ('\n',cur.fetchall())

# Maths aggregate
# AVG, MIN, MAX, COUNT, SUM

# Sum all
cur.execute('SELECT SUM (Population) FROM PopByRegion')
print ('\n',cur.fetchall())

# Sum by group
cur.execute('SELECT SUM (Population) FROM PopByCountry GROUP BY Region')
print ('\n',cur.fetchall())

# Self joins allow comparisons within a table, e.g. find all pairs with close pop
cur.execute('''
    SELECT A.Country, B.Country 
    FROM   PopByCountry A INNER JOIN PopByCountry B 
    WHERE  (ABS(A.Population - B.Population) <= 1000)
    AND    (A.Country != B.Country)''')

print ('\n',cur.fetchall())

# Nested queries

cur.execute('''
    SELECT DISTINCT Region 
    FROM PopByCountry 
    WHERE Region NOT IN 
        (SELECT DISTINCT Region 
         FROM PopByCountry 
         WHERE (PopByCountry.Population = 8764))
    ''')
        
print ('\n',cur.fetchall())

con.close() # close database