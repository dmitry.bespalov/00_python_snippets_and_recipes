import tensorflow as tf

# A tf.placeholder is a node in a TensorFlow graph,
# Values may be passed to Placeholders during a session

a = tf.placeholder(tf.int32)
b = tf.placeholder(tf.int32)
c = tf.add(a, b, name = 'add')
values = {a:5, b:3}

# Start a session (initialises graph)

sess = tf.Session()

# Define graph summary
summary_writer = tf.summary.FileWriter('./temp/1', sess.graph)

sess.run([c], values)

# Then go to linux command line and type
# tensorboard --logdir=temp/1
# Open provided link