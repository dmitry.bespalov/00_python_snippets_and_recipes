
## IMPORT LIBRARIES
## ----------------
import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

## LOAD DATA
## ---------
data = pd.read_pickle('imdb_numbered.p')

X = list(data.numbered_text.values)
y = data.sentiment.values


## MAKE ALL X DATA THE SAME LENGTH
# We will use keras to make all X data a length of 512
# Shorter data will be padded with 0, longer data will be truncated
# We have oreviously kept the value zero free from use

processed_X = \
    keras.preprocessing.sequence.pad_sequences(X,
                                               value=0,
                                               padding='post',
                                               maxlen=512)

## SPLIT DATA INTO TRAINIGN AND TEST SETS

X_train,X_test,y_train,y_test=train_test_split(
        processed_X,y,test_size=0.2,random_state=999)


# BUILD MODEL

# input shape is the vocabulary count used for the movie reviews# 
# (10,000 words plus our zero padding)
vocab_size = 10001

###############################################################################
# Info on neural network layers
#
# The layers are stacked sequentially to build the classifier:
#
# The first layer is an Embedding layer. This layer takes the integer-encoded 
# vocabulary and looks up the embedding vector for each word-index. These 
# vectors are learned as the model trains. The vectors add a dimension to the 
# output array. The resulting dimensions are: (batch, sequence, embedding).
#
# Next, a GlobalAveragePooling1D layer returns a fixed-length output vector for
# each example by averaging over the sequence dimension. This allows the model 
# to handle input of variable length, in the simplest way possible.
#
# This fixed-length output vector is piped through a fully-connected (Dense) 
# layer with 16 hidden units.
#
# The last layer is densely connected with a single output node. Using the 
# sigmoid activation function, this value is a float between 0 and 1, 
# representing a probability, or confidence level.
##############################################################################


model = keras.Sequential()
model.add(keras.layers.Embedding(vocab_size, 16))
model.add(keras.layers.GlobalAveragePooling1D())
model.add(keras.layers.Dense(16, activation=tf.nn.relu))
model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid))

model.summary()

# CONFIGURE OPTIMIZER

model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='binary_crossentropy',
              metrics=['accuracy'])

# CREATE A VALIDATION SET
# Will will split our training set into trainign and validation sets.
# We can then tune the model while keeping our original test data unused
# until we want to test the final model.

x_val = X_train[:10000]
partial_x_train = X_train[10000:]

y_val = y_train[:10000]
partial_y_train = y_train[10000:]

# TRAIN THE MODEL

history = model.fit(partial_x_train,
                    partial_y_train,
                    epochs=250,
                    batch_size=512,
                    validation_data=(x_val, y_val),
                    verbose=1)



