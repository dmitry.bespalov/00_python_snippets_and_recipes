"""
Return the sum of the first n numbers using a recursive method.

Recursion adds all the necessary call to the stack and then works backwards.
"""

def number_sum(n):
    # Avoid negative numbers
    assert(n >= 0), 'n must be at least 0'
    
    # recursive method
    if n == 0:
        print ('End recurssion')
        return 0 
    else:
        print ('Recurssion on', n)
        return n + number_sum(n-1)
    

# Test function
print (number_sum(10))