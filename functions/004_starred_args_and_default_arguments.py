#python
#*args

*args allows you to pass an arbitrary number of arguments to a function.
E.g. 

def function(named_arg,*args):
 print(named_arg)
 print(args)

function(1,2,3,4,5)
>>>
1
(2,3,4,5)

*args is just a convention. Other *names may be used.

args is accessed as a tuple.

See also **kwargs for creating dictionaries from values passed to a function


Default values may be assigned in a function, e.g. 

def function(x,y,food="spam")

Default variables must come after non-defaulted variables.
