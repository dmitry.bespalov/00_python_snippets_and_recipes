#python

In Python all variables declared in the main programme are globally readable (they can be accessed by all functions), but can only be changed in a function if 'global' is called. If 'global' is not used a local variable with the same name is established.


def change_global():
    global a # function can now change the value of a defined in main programme 
    a=20 # the main program a is changed
    b=10 # a local copy of b is created and used because no global call has been made
    return

a=50
b=100
print('Before function',a,b)
change_global()
print('After function',a,b)

Output:

Before function 50 100
After function 20 100
