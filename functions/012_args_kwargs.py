"""
Functions showing use of args and kwargs
Note, order is always:
    1) Required args
    2) Default args
    3) Optional *args
    4) Optional **kwargs
"""



def f(x, y=1, *args):
    """
    Function using required argument x, default argument y, and optional args
    """
    print ('x: ', x)
    print ('y: ',y)
    for i, arg in enumerate(args):
        print ('arg', i, ': ', arg)
    
    return 0
        
f(1,2,3,4,5)
print()
f(2)

def g(**kwargs):
    """Function iterating through optional kwargs"""
    for key, value in kwargs.items():
        print (key, value)
        
    return 0

g(name="Fred", legs=4)
g()