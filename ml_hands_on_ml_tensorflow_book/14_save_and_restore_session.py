"""Linear regression. Not a neural net.

Save needs to be performed on an opend session"""

import tensorflow as tf
import numpy as np
from sklearn.datasets import fetch_california_housing
from sklearn.preprocessing import StandardScaler

# to make this code's output stable across runs
def reset_graph(seed=42):
    tf.reset_default_graph()
    tf.set_random_seed(seed)
    np.random.seed(seed)

# Get California housing data 
housing = fetch_california_housing()

# Create scaled data set
scaler = StandardScaler()
scaled_housing_data = scaler.fit_transform(housing.data)

# Add a bias column, X0, (allows for 'x' in y=mx+c)
m, n = housing.data.shape
scaled_housing_data_plus_bias = np.c_[np.ones((m, 1)), scaled_housing_data]

# Set up Gradient Descent TensorFlow

reset_graph()

n_epochs = 1000
learning_rate = 0.01

X = tf.constant(scaled_housing_data_plus_bias, dtype=tf.float32, name="X")
y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name="y")
theta = tf.Variable(
        tf.random_uniform([n + 1, 1], -1.0, 1.0, seed=42), name="theta")
y_pred = tf.matmul(X, theta, name="predictions")
error = y_pred - y
mse = tf.reduce_mean(tf.square(error), name="mse")

# Set up optimizer

#optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)

# Alternative optimizer (often faster)
optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate,
                                       momentum=0.9)

training_op = optimizer.minimize(mse)

# Run tensorflow, and return mean squared error

init = tf.global_variables_initializer()

# Set up saver
saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run(init)

    for epoch in range(n_epochs):
        if epoch % 100 == 0:
            print("Epoch", epoch, "MSE =", mse.eval())
        sess.run(training_op)
    
    best_theta = theta.eval()
    save_path = saver.save(sess, "saved_model/test_model_save.ckpt")

print("Best theta:")
print(best_theta)


# To restore a model replace the sess.run.init with saver.resttore: 
with tf.Session() as sess:
    saver.restore(sess, "saved_model/test_model_save.ckpt")
    print(theta.eval())
    
# Note: If you want to have a saver that loads and restores theta with a 
# different name, such as "weights"
# saver = tf.train.Saver({"weights": theta})
    
"""By default the saver also saves the graph structure itself in a second file 
with the extension .meta. You can use the function tf.train.import_meta_graph()
to restore the graph structure. This function loads the graph into the default
graph and returns a Saver that can then be used to restore the graph state 
(i.e., the variable values).

This means that you can import a pretrained model without having to have the 
corresponding Python code to build the graph. This is very handy when you keep 
tweaking and saving your model: you can load a previously saved model without 
having to search for the version of the code that built it."""

reset_graph()
# notice that we start with an empty graph.

saver = tf.train.import_meta_graph("/tmp/my_model_final.ckpt.meta")  # this loads the graph structure
theta = tf.get_default_graph().get_tensor_by_name("theta:0") # not shown in the book

with tf.Session() as sess:
    saver.restore(sess, "/tmp/my_model_final.ckpt")  # this restores the graph's state
    print(theta.eval())