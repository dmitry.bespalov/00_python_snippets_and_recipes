import numpy as np
import matplotlib.pyplot as plt

################################################################################
################ Create  and plot linear data with noise #######################
################################################################################

m = 100 # number of samples

X = 2 * np.random.rand(m, 1)
y = 4 + 3 * X + np.random.randn(100,1)
plt.scatter(X, y)
plt.title('Raw data')
plt.show()

################################################################################
############### Linear regression using the Normal Equation ####################
################################################################################

X_b = np.c_[np.ones((100,1)), X] # Add x0 to each instance
theta_best = np.linalg.inv(X_b.T.dot(X_b)).dot(X_b.T).dot(y)
print ('Normal equation best theta (theoretical = 4.0, 3.0):')
print (theta_best.T) # use .T to print as row

# Make predictios
y_predict = X * theta_best[1] + theta_best[0]

# Replot with prediction
plt.scatter(X, y, label='Actual')
plt.scatter(X, y_predict, marker='x', color='k', label='Predicted')
plt.legend()
plt.title('Linear regression with Normal Equation')
plt.show()

################################################################################
##################### Linear regression using sklearn ##########################
################################################################################

# Sklearn linear regression uses the Normal Equation but is more computationally
# efficient (lower big O)

from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)
intercept = lin_reg.intercept_
coeffs = lin_reg.coef_
print ('\nSklearn intercept and coefficients:')
print (intercept, coeffs)

# Predict and plot
y_predict = lin_reg.predict(X)
plt.scatter(X, y, label='Actual')
plt.scatter(X, y_predict, marker='x', color='k', label='Predicted')
plt.legend()
plt.title('Linear regression using sklearn linear regression')
plt.show()

################################################################################
############ Linear regression using Batch Gradient Descent ####################
################################################################################

# Explicit solutions to linear regression struggle with very large data sets.
# ML offers alternatives. Data should be normalised to even out descent over 
# multiple features. Batch gradient descent works on all data at one time

eta = 0.1 # learning rate
n_iterations = 250
theta = np.random.randn(2,1) # random inisialization

# Calculate best theta for batch, but only corrects overall theta by batch theta
# * learning rate

for i in range(n_iterations):
    gradients = 2/m * X_b.T.dot(X_b.dot(theta) -y)
    theta = theta - (eta * gradients)

print ('\nBatch gradient descent intercept and coefficients:')
print (theta.T)

################################################################################
########## Linear regression using Stochastic Gradient Descent #################
################################################################################

# Stochastic gradient descent  selects a asingle sample at a time to adjust 
# theta. It is much faster than using all data (batch) but never reaches a true
# optimum.

n_epochs = 50
t0, t1 = 5, 50 # learning schedule hyperparemters

def learning_schedule(t):
    return t0 / (t + t1)

theta = np.random.randn(2, 1) # random initiation

for epoch in range(n_epochs):
    for i in range(m):
        random_index = np.random.randint(m)
        xi = X_b[random_index:random_index+1]
        yi = y[random_index:random_index+1]
        gradients = 2 * xi.T.dot(xi.dot(theta) - yi)
        eta = learning_schedule(epoch * m + i)
        theta -= eta * gradients

# Predict and plot
y_predict = X * theta_best[1] + theta_best[0]
plt.scatter(X, y, label='Actual')
plt.scatter(X, y_predict, marker='x', color='k', label='Predicted')
plt.legend()
plt.title('Linear regression using stochastic gradient decsent')
plt.show()

print ('\nStochastic gradient descent intercept and coefficients:')
print (theta.T)

################################################################################
####### Linear regression using sklearn Stochastic Gradient Descent ############
################################################################################

from sklearn.linear_model import SGDRegressor
sgd_reg = SGDRegressor(max_iter=50, tol=1e-3, penalty=None, eta0=0.1)
sgd_reg.fit(X, y.ravel()) # ravel flattens column vector to row vector
intercept = sgd_reg.intercept_
coeffs = sgd_reg.coef_
print ('\nSklearn stochastic gradient descent intercept and coefficients:')
print (intercept, coeffs)

# Mini batch gradient descent uses small random batches of data. This can use
# processors esp a GPU more efficiently than single stochastic gradient descent.
# Minibatch gradient descent can be performed with the parital_fit method in
# SGDRegressor.

