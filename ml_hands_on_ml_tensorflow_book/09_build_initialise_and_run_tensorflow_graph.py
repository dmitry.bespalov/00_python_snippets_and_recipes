"""
A tensorflow programme is generally split into two parts:
    1) Build computation graph
    2) Exxcute computation graph
"""
import tensorflow as tf

# Define Tensotflow variables (graph inputs).
# These are not iitialisaed until the tensor is run
x = tf.Variable(3, name="x")
y = tf.Variable(4, name="y")

# Define the graph. This does not calculate the answer yet
f = x*x*y + y + 2

# Initialise and run tensorflow session
sess = tf.Session()
sess.run(x.initializer)
sess.run(y.initializer)
result = sess.run(f)
print ('Result:', result)
sess.run(x.initializer)

# Alternative initialisation using 'with')

with tf.Session() as sess:
    x.initializer.run()
    y.initializer.run()
    result = f.eval()
    print ('Result:', result)
    
# Or to set up all variables together
init = tf.global_variables_initializer()
with tf.Session() as sess:
    init.run()
    result = f.eval()
    print ('Result:', result)