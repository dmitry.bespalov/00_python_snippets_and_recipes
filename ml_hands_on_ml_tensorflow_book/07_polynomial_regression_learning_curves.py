# Polynomial fitting may use linear methods, but we create polynomial features,
# that is powers of each feature

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

################################################################################
################ Create  and plot linear data with noise #######################
################################################################################

m = 100 # number of samples

X = 6 * np.random.rand(m, 1) - 3
y = 0.5 * X**2 + X + 2 + np.random.randn(m, 1)

################################################################################
############### Use sklearn to create polynomia; features ######################
################################################################################

from sklearn.preprocessing import PolynomialFeatures
poly_features = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly_features.fit_transform(X)
print ('Original X[0]:', X[0])
print ('Polynomial X[0]:', X_poly[0])

################################################################################
#################  Split into training and test data sets ######################
################################################################################

X_train, X_val, y_train, y_val = train_test_split(
        X_poly, y, test_size=0.2, random_state=1)

################################################################################
###############  Fit polynomial data with linear regression ####################
################################################################################

# Beware overfitting with polynomial regression

model = LinearRegression()

train_errors, val_errors = [], []
for m in range(1, len(X_train)): # Use successively larger par of training set
    model.fit(X_train[:m], y_train[:m])
    y_train_predict = model.predict(X_train[:m])
    y_val_predict = model.predict(X_val)
    train_errors.append(mean_squared_error(y_train[:m], y_train_predict))
    val_errors.append(mean_squared_error(y_val, y_val_predict))
    
    
################################################################################
###########################  Plot learning curves ##############################
################################################################################
    
# Examine the effect of changing the number of polynomial features
# An overfitted model will perform better on training than validation
# Increasing polynomial features reduces bias (error caused by model being too
# simple), but increases variance (error caused by over-fitting)    


plt.plot(np.sqrt(train_errors), "r-+", linewidth=2, label="train")
plt.plot(np.sqrt(val_errors), "b-", linewidth=3, label="val")
plt.legend(loc="upper right", fontsize=14)
plt.xlabel("Training set size", fontsize=14)
plt.ylabel("RMSE", fontsize=14)
plt.show() 