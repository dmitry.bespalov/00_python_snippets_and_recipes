# Polynomial fitting may use linear methods, but we create polynomial features,
# that is powers of each feature

import numpy as np
import matplotlib.pyplot as plt

################################################################################
################ Create  and plot linear data with noise #######################
################################################################################

m = 100 # number of samples

X = 6 * np.random.rand(m, 1) - 3
y = 0.5 * X**2 + X + 2 + np.random.randn(m, 1)

plt.scatter(X, y)
plt.title('Raw data')
plt.show()

################################################################################
############### Use sklearn to create polynomia; features ######################
################################################################################

from sklearn.preprocessing import PolynomialFeatures
poly_features = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly_features.fit_transform(X)
print ('Original X[0]:', X[0])
print ('Polynomial X[0]:', X_poly[0])

################################################################################
###############  Fit polynomial data with linear regression ####################
################################################################################

# Beware overfitting with polynomial regression

from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X_poly, y)
intercept = lin_reg.intercept_
coeffs = lin_reg.coef_
print ('\nSklearn intercept and coefficients:')
print (intercept, coeffs)

# Predict and plot
y_predict = lin_reg.predict(X_poly)
plt.scatter(X, y, label='Actual')
plt.scatter(X, y_predict, marker='x', color='k', label='Predicted')
plt.legend()
plt.title('Linear regression using sklearn linear regression')
plt.show()