#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings('ignore')

"""mnist digit recognition"""




import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import StratifiedKFold
from sklearn.base import clone
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix

# Load data

train = pd.read_csv('mnist/mnist_train_10k.csv', header=None).values
X_train = train [:, 1:]
y_train = train[:,0]
shuffle_index = np.random.permutation(train.shape[0])
X_train = X_train[shuffle_index]
y_train = y_train[shuffle_index]

test = pd.read_csv('mnist/mnist_test.csv', header=None).values
X_test = test[:, 1:]
y_test = test[:,0]

#  Show example
#print ('\nLabel: %d' %(y_train[50]))
#example = X_train[50].reshape(28,28)
#plt.imshow(example)
#plt.axis('off')
#plt.show()

## BUILD A CLASSIFIER FOR THE NUMBER 5

y_train_5 = y_train == 5
y_test_5 = y_test == 5

# Train a Stochastic Gradient Descent classifier
from sklearn.ensemble import RandomForestClassifier
model = RandomForestClassifier(random_state=42)

skfolds = StratifiedKFold(n_splits=3, random_state=42)

for train_index, test_index in skfolds.split(X_train, y_train_5):
    X_train_fold = X_train[train_index]
    y_train_fold = y_train_5[train_index]
    X_test_fold = X_train[test_index]
    y_test_fold = y_train_5[test_index]
    model_clone = clone(model)
    model_clone.fit(X_train_fold, y_train_fold)
    y_pred = model_clone.predict(X_test_fold)
    n_correct = sum (y_pred == y_test_fold)
    frac_correct = n_correct / len(y_pred)
    print ('Accuracy:', frac_correct)
    tp = sum((y_pred == 1) & (y_test_fold == 1))
    fp = sum((y_pred == 1) & (y_test_fold == 0))
    tn = sum((y_pred == 0) & (y_test_fold == 0))
    fn = sum((y_pred == 0) & (y_test_fold == 1))
    precision = tp / (tp + fp) #  Accuracy of positive predictions
    recall = tp / (tp + fn) # Proportion of true positives detected
    f1 = 2 / (1/precision + 1/recall)
    print ('Precision:', precision)
    print ('Recall:', recall)
    print ('F1:', f1)

## Or using cross validation scoring shortcut
#print(cross_val_score(model, X_train, y_train_5, cv=3, scoring='accuracy'))
#
## Confusion matrix
#y_train_pred = cross_val_predict(model, X_train, y_train_5, cv=3)
#print (confusion_matrix(y_train_5,y_train_pred))
    
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
#model.fit(X_train, y_train)
y_probas = cross_val_predict(model, X_train, y_train_5, cv=3, 
                             method="predict_proba")
y_scores = y_probas[:,1] # Probability of positive class
fpr, tpr, thresholds = roc_curve(y_train_5, y_scores)
plt.plot(fpr, tpr)
plt.plot([0,1],[0,1], 'k--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.show()
print ('AUC:', roc_auc_score(y_train_5, y_scores))
    


