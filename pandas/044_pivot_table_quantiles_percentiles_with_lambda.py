#pivot
#percentile
#quantile
#pandas
#python


df = pd.DataFrame([['a',2,3],
                             ['a',5,6],
                             ['a',7,8], 
                             ['b',9,10], 
                             ['b',11,12], 
                             ['b',13,14]], columns=list('abc'))

np.percentile seems to work just fine?

In [140]: df.pivot_table(columns='a', aggfunc=lambda x: np.percentile(x, 50))
Out[140]: 
a  a   b
b  5  11
c  6  12


