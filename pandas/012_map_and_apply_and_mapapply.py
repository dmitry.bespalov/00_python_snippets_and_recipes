#python
#pandas
#map
#apply
#applymap

Map: It iterates over each element of a series.
df[�column1�].map(lambda x: 10+x), this will add 10 to each element of column1.
df[�column2�].map(lambda x: �AV�+x), this will concatenate �AV� at the beginning of each 
element of column2 (column format is string).

Apply: As the name suggests, applies a function along any axis of the DataFrame.
df[[�column1�,�column2�]].apply(sum), it will returns the sum of all the values of 
column1 and column2.

ApplyMap: This helps to apply a function to each element of dataframe.
func = lambda x: x+2
df.applymap(func), it will add 2 to each element of dataframe (all columns of 
dataframe must be numeric type)


Series examples
===============

import pandas as pd

songs_66=pd.Series([3,1,11,15],
                   index=['George','Ringo','John','Paul'],
                   name='Counts')


def format(x):
    if x==1:
        template = '{} song'
    else:
        template = '{} songs'
    return template.format(x)

print(songs_66.map(format))

# .map can also take a dictionary and update any value with matching key to thatdictionary

new_val={3: None,11:21} # this may be formatted as dictionary or series containing dictionary
print(songs_66.map(new_val)) # note values are lost if no equivalent in new dictionary

# .apply is similar to .map but only works with functions, not with value dictionaries



Dataframe examples
==================

apply
-----

Another frequent operation is applying a function on 1D arrays to each column or row. 
DataFrame�s apply method does exactly this:

In [116]: frame = DataFrame(np.random.randn(4, 3), columns=list('bde'), index=['Utah', 'Ohio', 'Texas', 'Oregon'])

In [117]: frame
Out[117]: 
               b         d         e
Utah   -0.029638  1.081563  1.280300
Ohio    0.647747  0.831136 -1.549481
Texas   0.513416 -0.884417  0.195343
Oregon -0.485454 -0.477388 -0.309548

In [118]: f = lambda x: x.max() - x.min()

In [119]: frame.apply(f)
Out[119]: 
b    1.133201
d    1.965980
e    2.829781
dtype: float64


applymap
--------

Many of the most common array statistics (like sum and mean) are DataFrame methods, 
so using apply is not necessary.

Element-wise Python functions can be used, too. 
Suppose you wanted to compute a formatted string from each floating point value in frame. 
You can do this with applymap:

In [120]: format = lambda x: '%.2f' % x

In [121]: frame.applymap(format)
Out[121]: 
            b      d      e
Utah    -0.03   1.08   1.28
Ohio     0.65   0.83  -1.55
Texas    0.51  -0.88   0.20
Oregon  -0.49  -0.48  -0.31


map
---

The reason for the name applymap is that Series has a map method for applying an element-wise function:
In [122]: frame['e'].map(format)
Out[122]: 
Utah       1.28
Ohio      -1.55
Texas      0.20
Oregon    -0.31
Name: e, dtype: object


summary
-------

Summing up, apply works on a row / column basis of a DataFrame, 
applymap works element-wise on a DataFrame, 
and map works element-wise on a Series.


