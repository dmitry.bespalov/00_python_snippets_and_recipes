#python
#pandas
#loc
#iloc


import pandas as pd

george=pd.Series([10,7],
                 index=['1968','1969'],
                  name='George songs')

print(george)
print(george.index) # note dtype=object


#Check whether an index is unique
dupe=pd.Series([10,2,7,1],
               index=['1968','1968','1969','1970'],
               name='George songs')
print()
print('Is index unique? ',dupe.index.is_unique)

# using index numbers
print()
print('First: ',george[0]) # first item
print('Last: ',george[-1]) # last item

# But better practice is to use iloc for index ('row') number
print()
print('First iloc: ',george.iloc[0]) # first item
print('Last: iloc ',george.iloc[-1]) # last item

# And loc is better practice than simply using key alone
# george.loc['1968'] is better than george['1968'] which is confused with column names

# iloc and loc slices may be used:
print('\nSlices')
print(dupe.iloc[0:1]) # note range here will just print first (zero) item
print(dupe.iloc[[0,2]]) # list passed to pandas
print(dupe.loc[['1968','1970']])
print(dupe.loc['1969':])

# at and iat are similar to loc and iloc but return numpy arrays. They can only be used for a single index
x=dupe.at['1968']
print('\nat 1968: ',x)

# .ix is similar to [] indexing in that it tries to support, and look in, both positional and label indexing
# .ix is best avoided, but may be useful when herirachial indexes are mixed label and positional
