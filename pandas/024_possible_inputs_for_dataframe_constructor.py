#python
#pandas

* 2D data array (passing optional row and colum labels).
Could be NumPy array or list of lists or tuples.

* NumPy masked array. Masked values become NA/missing in dataframe

* dictionary of arrays, lists or tuples (each dictionary becomes a column).
All sequences must be the same length

* NumPy structured/record array 

* dictionary of series

* list of dictionaries (each item is new row)

* another DataFrame