#python
#pandas

import pandas as pd

songs_66 = pd.Series([3,None,11,9],
                     index=['George','Ringo','John','Paul'],
                     name='Counts') 

songs_69 = pd.Series([18,22,7,5],
                     index=['John','Paul','George','Ringo'],
                     name='Counts')



# Replace missing numbers with zero
print('\nReplace NaN with zero')
songs_66=songs_66.fillna(0)
print(songs_66)     

 # using .get to return a value with a default value if not found
print('Using get')
print(songs_66.get('George','missing'))
print(songs_66.get('Heorge','missing'))