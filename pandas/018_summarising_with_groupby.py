#python
#pandas
#groupby
#pivot
#statistics

"""groupby and pivot_table can both be used for summarising.
output format can be different between pivot and groupby
groupby always put 'summarise by' columns on left
groupby allows for complex coe to be applies to each group created"""



import pandas as pd

scores=pd.DataFrame({
    'name':['Adam','Bob','Dave','Fred'],
    'age':[15,16,16,15],
    'test1':[95,81,89,None],
    'test2':[80,82,84,88],
    'teacher':['Ashby','Ashby','Jones','Jones']})

# Get median by teacher
print(scores.groupby('teacher').median())
print()

# Instead of built in functions like median we can use .apply
# This can be done using a lambda function
print(scores.groupby('teacher').apply(lambda x: x.median()))
print()

# To restrict columns:
print(scores.groupby('teacher').median()[['test1','test2']])
print()

# Group by two columns (creates a multi-level index):
print(scores.groupby(['teacher','age']).median()[['test1','test2']])
print()

# Output more than one summary value using .agg
print(scores.groupby(['teacher','age']).agg([min,max])[['test1','test2']])
print()

# Groupby functions:
# .all # Boolean True if all true
# .any # Boolean
# .count # count of non null values
# .size # size of group including null values
# .max
# .min
# .mean
# .median
# .sem
# .std
# .var
# .sum
# .prod
# .idxmax # index of max values
# .idxmin # index of min values
# .quantile
# .agg(functions)
# .apply(func)
# .last # last value
# .nth # nth row of group
