"""Google deep mind neural net as published in the original paper.
Three convolutional layers followerd by two fully connected layers. All layers
are separated by ReLU. The output of the model is Q values for every action
available in the environment.

Input tensor is 4D: batch size, colour (stack of subsequent frames), and two
image dimensions.

The output of the convolutional layers is a 4D Tensor. This is converted to a 
2D tensor (bastch size, and flattened output) by the forward method which 
pulls the whole network together."""


import torch
import torch.nn as nn

import numpy as np


class DQN(nn.Module):
    def __init__(self, input_shape, n_actions):
        super(DQN, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(input_shape[0], 32, kernel_size=8, stride=4),
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=4, stride=2),
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),
            nn.ReLU()
        )

        conv_out_size = self._get_conv_out(input_shape)
        self.fc = nn.Sequential(
            nn.Linear(conv_out_size, 512),
            nn.ReLU(),
            nn.Linear(512, n_actions)
        )

    def _get_conv_out(self, shape):
        """Converts output of convolutional layers to required size of a fully
        connected layers"""
        o = self.conv(torch.zeros(1, *shape))
        return int(np.prod(o.size()))

    def forward(self, x):
        """Reshape 4D tensor to 1D tensor using the view method of tensors,
        
        e.g. A 3D tensor of shape (2,3,4) can be converted to a 2D tensor of 
        shape (6.4) with T.view(6,4)
        
        -1 is a wildcard, which will automatically populate
        so the above would also work with T.view(-1, 4) or T.view(6,-1)"""
        conv_out = self.conv(x).view(x.size()[0], -1)
        return self.fc(conv_out)
