# need to pip install gym[atari]

"""
       
General method:
    
    Overall aim is to create a neutal network that predicts Q. Improvement 
    comes from improvd accuract in predicting 'current' understood Q, and in
    revealing more about Q as knowledge is gained (some rewards only discovered
    after time). The target network is updated every 1000 frames, which is 
    about an epsiode of pong.
    
    Note: Training network is updated every batch. Target network is
          updated periodically. Improving training network improves prediction
          of Q against the target standard. Updating the target standard 
          periodically allows the target standard to improve over time.
    
    1)  Initial parameters for Q(s,a) with random weights and empty the replay
        buffer
    2)  With probability epsilon select a random action, or best current 
        action, argmax Q(s,a). Best current action from training network.
    3)  Execute action a, and observe reward r and next state s'.
    4)  Store transition (s,a,r,s') in the replay buffer
    5)  Sample random minibatch of transitions from the replay buffer. Random
        sampling from the replay buffer avoids autocorrelation problems that 
        can occur if using current state net
    6)  For every transition in the buffer, calculate y=r if the episode has
        ended at this step, or y=r + gamma(max Q) from target network otherwise
    7)  Calculate loss: l = (Q[s,a]-y)**2
    8)  Update Q(s,a) using SGD (stochastic gradient descent) by minimising the
        loss in respect to model parameters, i.e. model parameters predict loss
    9)  Every n steps copy weights to target network
    10) Repeat from (2)

Neural network enhanced by:
    
    1) EPSILON-GREEDY - Epsilon is the proportion of actions that are random.
       Model moves from high epsilon to low (2-5%, 'greedy')
    2) REPLAY BUFFER - Holds a large buffer of previous episodes which 
       increases diversity of sampling
    3) TARGET NETWORK - is updated every n steps (often 1000) to avoid high
       autocorrelation between individual steps which can cause problems 
       improving the network. Target network (infrequently updated) is used to
       select actions.
    4) 'k' - by combining k (e.g. 4) frames from subsequent time-steps the 
        network can 'sense' change (e.g. velocity) as a single game state.
"""



#!/usr/bin/env python3

# 'Wrapper' provides Atart-specific methods (e.g. splitting game into lives,
# scaling images, coping with Atari flickering, only using every 3-4 sceen 
# images, stacking four images together, scaling rewards to -1, 0, 1)
import wrappers 

import dqn_model # neural network based on the original Deep Mind paper

import argparse
import time
import numpy as np
import collections

import torch
import torch.nn as nn
import torch.optim as optim


DISPLAY_ON_SCREEN = True

# Set hyperparameters

DEFAULT_ENV_NAME = "PongNoFrameskip-v4"
MEAN_REWARD_BOUND = 19.5 # reward when training will end (mean of 100 games)

GAMMA = 0.99 # Bellam gamma
BATCH_SIZE = 32 # batch size to sample from replay buffer
REPLAY_SIZE = 10000 # max caapcity of replay buffer
LEARNING_RATE = 1e-4 # Learning rate of Adam neural net optimizer
SYNC_TARGET_FRAMES = 1000 # Sync interval betweem training and replay model
REPLAY_START_SIZE = 10000

EPSILON_DECAY_LAST_FRAME = 10**5 #Epsilon decays of 10k frames
EPSILON_START = 1.0
EPSILON_FINAL = 0.001

# Stroage for a single replay  as a named tuple
Experience = collections.namedtuple(
        'Experience', 
        field_names=['state', 'action', 'reward', 'done', 'new_state'])


class ExperienceBuffer:
    def __init__(self, capacity):
        # deque allows easy popping from both ends. Aoutomatically manages capacity
        self.buffer = collections.deque(maxlen=capacity)

    def __len__(self):
        return len(self.buffer)

    def append(self, experience):
        self.buffer.append(experience)

    def sample(self, batch_size):
        indices = np.random.choice(len(self.buffer), batch_size, replace=False)
        states, actions, rewards, dones, next_states = zip(*[self.buffer[idx] for idx in indices])
        return np.array(states), np.array(actions), np.array(rewards, dtype=np.float32), \
               np.array(dones, dtype=np.uint8), np.array(next_states)


class Agent:
    def __init__(self, env, exp_buffer):
        self.env = env
        self.exp_buffer = exp_buffer
        self._reset()

    def _reset(self):
        self.state = env.reset()
        self.total_reward = 0.0

    def play_step(self, net, epsilon=0.0, device="cpu"):
        done_reward = None

        if np.random.random() < epsilon:
            # Get random action
            action = env.action_space.sample()
        else:
            # Get state as an array
            state_a = np.array([self.state], copy=False)
            # Converyto Tensor
            state_v = torch.tensor(state_a).to(device)
            # Get Q values for state from 'net' (past model)
            q_vals_v = net(state_v)
            # Get best action
            _, act_v = torch.max(q_vals_v, dim=1)
            action = int(act_v.item())

        # do step in the environment
        new_state, reward, is_done, _ = self.env.step(action)
        if DISPLAY_ON_SCREEN:
            env.render()
        self.total_reward += reward

        # Record step in replay buffer        
        exp = Experience(self.state, action, reward, is_done, new_state)
        self.exp_buffer.append(exp)
        self.state = new_state
        if is_done:
            done_reward = self.total_reward
            self._reset()
        return done_reward


def calc_loss(batch, net, tgt_net, device="cpu"):
    """Optimised for parallel processing"""
    states, actions, rewards, dones, next_states = batch # batch is zip object

    states_v = torch.tensor(states).to(device)
    next_states_v = torch.tensor(next_states).to(device)
    actions_v = torch.tensor(actions).to(device)
    rewards_v = torch.tensor(rewards).to(device)
    done_mask = torch.ByteTensor(dones).to(device)

    # Pass observations to model and extract Q values for thise actions with gather()
    # '1' is dimension 1 which relates to actions taken
    # Unsqueeze and squeeze are required for gather and to eliminate unwanted dimensions
    # Chose action from current network (net) 
    state_action_values = net(states_v).gather(1, actions_v.unsqueeze(-1)).squeeze(-1) # netwrok that we're training
    # Get max (which is both max and argmax) of Q-values for action from target network
    next_state_values = tgt_net(next_states_v).max(1)[0] # get next state values from target network (updated preiodically)
    next_state_values[done_mask] = 0.0 # Prevents discounted reqard for last step. Required.
    next_state_values = next_state_values.detach() # detach prevents gradients from flowing into taregt networks graph

    # Calculate Bellman approximation (predict Q) and the mean squared error loss
    expected_state_action_values = next_state_values * GAMMA + rewards_v
    return nn.MSELoss()(state_action_values, expected_state_action_values)


if __name__ == "__main__":
    # The parser allows us to run command line arguments (helps use of cuda)
    parser = argparse.ArgumentParser()
    parser.add_argument("--cuda", default=False, action="store_true", help="Enable cuda")
    parser.add_argument("--env", default=DEFAULT_ENV_NAME,
                        help="Name of the environment, default=" + DEFAULT_ENV_NAME)
    parser.add_argument("--reward", type=float, default=MEAN_REWARD_BOUND,
                        help="Mean reward boundary for stop of training, default=%.2f" % MEAN_REWARD_BOUND)
    args = parser.parse_args()
    device = torch.device("cuda" if args.cuda else "cpu")

    # Create environment 
    env = wrappers.make_env(args.env)

    # set up training (net) and raget nn. Both have same structure
    net = dqn_model.DQN(env.observation_space.shape, env.action_space.n).to(device)
    tgt_net = dqn_model.DQN(env.observation_space.shape, env.action_space.n).to(device)
    print(net)
    
    # Set up replay buffer and pass it to the agent with the environment
    buffer = ExperienceBuffer(REPLAY_SIZE)
    agent = Agent(env, buffer)
    
    # Initialise epsilon (chance of using random action)
    epsilon = EPSILON_START

    # Set up nn otimizer
    optimizer = optim.Adam(net.parameters(), lr=LEARNING_RATE)
    
    # Set up buffer for full episode rewards
    total_rewards = []
    
    # Set up cpunter for frames and variables that deal with speed
    frame_idx = 0
    ts_frame = 0
    ts = time.time()

    # Ste up variable for last mean rewards (and increse if new mean is better)    
    best_mean_reward = None

    # Training loop (by frame)
    while True:
        # Increment frame index
        frame_idx += 1
        
        # Reduce epsolin (down to minimum)
        epsilon = max(EPSILON_FINAL, EPSILON_START - frame_idx / EPSILON_DECAY_LAST_FRAME)

        # play step using current network and get reward (also record in the replay buffer)    
        reward = agent.play_step(net, epsilon, device=device)
        
        # Reward is not none only if it is the final step in the episode. In that case write to console and TensorBoard
        if reward is not None:
            total_rewards.append(reward)
            speed = (frame_idx - ts_frame) / (time.time() - ts)
            ts_frame = frame_idx
            ts = time.time()
            mean_reward = np.mean(total_rewards[-100:])
            print("%d: done %d games, mean reward %.3f, eps %.2f, speed %.2f f/s" % (
                frame_idx, len(total_rewards), mean_reward, epsilon,
                speed
            ))
            if best_mean_reward is None or best_mean_reward < mean_reward:
                torch.save(net.state_dict(), args.env + "-best.dat")
                if best_mean_reward is not None:
                    print("Best mean reward updated %.3f -> %.3f, model saved" % (best_mean_reward, mean_reward))
                best_mean_reward = mean_reward
            if mean_reward > args.reward:
                print("Solved in %d frames!" % frame_idx)
                # End training loop if solutions are good enough
                break
        
        # If replay buffer has not reached the required start size then repear loop (add another step)    
        if len(buffer) < REPLAY_START_SIZE:
            continue
        
        # Sync parameters from trainign network to current network every 1,000 frames
        if frame_idx % SYNC_TARGET_FRAMES == 0:
            tgt_net.load_state_dict(net.state_dict())

        # Optimise trainign network
        # Zero gradients
        optimizer.zero_grad()
        # Get a random bacth of samples from the replay buffer
        batch = buffer.sample(BATCH_SIZE)
        # Calculate loss
        loss_t = calc_loss(batch, net, tgt_net, device=device)
        # Optimize network
        loss_t.backward()
        optimizer.step()
    # Close tensorboard writer
