#!/usr/bin/env python3

"""
Policy-based learning (learn actions that lead to best outcomes without 
estimating value of individual actions).

Cross-entropy is simple and is suitable where rewards are frequent which do not
need complex multi-step policies to be learned.

This is a model-free method - the method does not build a model of the
environemnt. Model-based methods try to predict what the next observation
and/or rewrd will be. Model free simply connects observations to actions.

Model-based methods are more useful when there are deterministic rules, such
as in a board game.

This is a policy-based method which links observations to probability of 
action. ALternative is value-based where actions are linked to estimated value
and the action with the highest value chosen. 

"""


import gym
from collections import namedtuple
import numpy as np
from tensorflow import keras
from sklearn.preprocessing import OneHotEncoder

DISPLAY_ON_SCREEN = True

# Define number of episodes to run in a batch
BATCH_SIZE = 16
# Define cut-off for selection of best episodes
PERCENTILE = 70
# TensorFlow learning rate
LEARNING_RATE = 0.001

class Net():
    """A single layer ReLU network
    No softmax final used in cross-entropy method, but a softmax layer will be
    added elsewhere to convert output to probabiltites.
    """
    def __init__(self, observation_space, action_space):
        
        # Set up Tensorflow model        
        self.model = keras.Sequential()
        
        self.model.add(keras.layers.Dense(
                24, input_shape=(observation_space,), activation="relu"))
        
        self.model.add(keras.layers.Dense(
                24, activation="relu"))
        
        self.model.add(keras.layers.Dense(
                action_space, activation="softmax"))
        
        self.model.compile(
                loss="binary_crossentropy", 
                optimizer=keras.optimizers.Adam(lr=LEARNING_RATE))
        
        return
    
        def remember(self, state, action, reward, next_state, done):
            """state/action/reward/next_state/done"""
            self.memory.append((state, action, reward, next_state, done))
        
        

# Define helper classes to store steps and episodes
Episode = namedtuple('Episode', field_names=['reward', 'steps'])
EpisodeStep = namedtuple('EpisodeStep', field_names=['observation', 'action'])


def iterate_batches(env, net, batch_size):
    """This is a a generator/yoeld function. It yields a batch of episodes.
    It requires gym environment, the net used to convert input --> action,
    and the batch size
    """
        # Set up tracking variables
    batch = [] # list of episode instances
    episode_reward = 0.0 # reward for current episode
    episode_steps = [] # list of episode steps
    # Reset environemnt
    obs = env.reset()
    
    # Get number of observations returned for state
    observation_space = env.observation_space.shape[0]
    
    # Start environement loop
       
    while True:
        # Get probabilities of action for observation
        obs = np.reshape(obs, [1, observation_space])
        obs = np.float32(obs)
        act_probs = np.array(net.predict(obs))
        # Select action based on probability
        action = np.random.choice(len(act_probs[0]), p=act_probs[0])
        # Pass action to envrionment
        next_obs, reward, is_done, _ = env.step(action)
        # Option rander to screen
        if DISPLAY_ON_SCREEN:
            env.render()
        # Update episode total reward
        episode_reward += reward
        # Update list of episode steps
        episode_steps.append(EpisodeStep(observation=obs, action=action))
        # If episode is at an end:
        if is_done:
            # Add episode to batch
            batch.append(Episode(reward=episode_reward, steps=episode_steps))
            # Reset environment and  episode trackers
            episode_reward = 0.0
            episode_steps = []
            next_obs = env.reset()
            # If batch complete yield batch and reset batch tracker
            if len(batch) == batch_size:
                # yield passes control back to the function that called
                # this function, and then continues after the yield line
                yield batch
                batch = [] # Clean up batch new call after yield
        # If episode not finished update observation from current step
        obs = next_obs


def filter_batch(batch, percentile):
    """Calculates a boundary reward level based on percentile of rewards in
    batch. Concetenates all observations & actions for episodes which reach
    target reward.
    """
    
    rewards = list(map(lambda s: s.reward, batch))
    reward_bound = np.percentile(rewards, percentile)
    reward_mean = float(np.mean(rewards))

    train_obs = []
    train_act = []
    for example in batch:
        if example.reward < reward_bound:
            continue
        # Train obs is a list of observation arrays (4 obs each)
        train_obs.extend(map(lambda step: step.observation, example.steps))
        # Train act is a list of actions
        train_act.extend(map(lambda step: step.action, example.steps))
    

    return train_obs, train_act, reward_bound, reward_mean


if __name__ == "__main__":
    """main training loop code"""
    
    # Set up gym environemnt
    env = gym.make("CartPole-v0")
    
    # Optional video wrapper
    #env = gym.wrappers.Monitor(env, directory="mon", force=True)
    
    # Get number of observations from environemt(allows the env to change)
    obs_size = env.observation_space.shape[0]
    # Get number of actins from environemnt
    n_actions = env.action_space.n

    # Set up TensorFlow net
    net = Net(obs_size, n_actions)

    # Training loop by batches (for loop calls for each batch to run)
    for iter_no, batch in enumerate(iterate_batches(env, net.model, BATCH_SIZE)):
        # Filter and concatenate best episodes
        obs, acts, reward_b, reward_m = filter_batch(batch, PERCENTILE)
        
        for i in range(len(obs)):
            single_obs = obs[i]
            single_act = np.zeros(2)
            single_act[acts[i]] = 1
            single_act = np.array([single_act])
            net.model.fit(single_obs, single_act, verbose=0)

        # Pront abd record key metrics
        print("iteration=%.0f, reward_mean=%.1f" % (iter_no, reward_m))

        if reward_m > 199:
            print("Solved!")
            break

