#python
#collections
#count

# Counter from collections allows easy counting of words or numbers
# It replaces manual work with dictionaries
# It works with string input

from collections import Counter

sentance='Look into my eyes. Look into my eyes. Not around the eyes, into the eyes, the eyes, the eyes'

# The following strips out characters from the string and puts it all in lower case
# Strings are immutible so a new string is constrcuted
new_sentance=''
for char in sentance:
    if char not in "?.,!/;:":
        new_sentance+=char
new_sentance=new_sentance.lower()

words=new_sentance.split(' ')
word_counts=Counter(words) # creates a word count object that is essentially an object
top_three_words=word_counts.most_common(3)
print(top_three_words)

# Word counts may be adjust manually, e.g.
word_counts['eyes']+=1

# Or word counts may be updated by addition of a new string
more_sentance='look into the eyes again'
more_words=more_sentance.split(' ')
word_counts.update(more_words)
top_three_words=word_counts.most_common(3)
print(top_three_words)


# Addition and subtraction methods also exist
sentance2='look into the eyes again stupid'
words2=sentance2.split(' ')
word_counts2=Counter(words2)

print(word_counts)
print(word_counts+word_counts2)
print(word_counts-word_counts2)


# A example with numbers:
my_numbers=[1,1,2,3,4,5,5,5,6,7,8]
print(Counter(my_numbers))
