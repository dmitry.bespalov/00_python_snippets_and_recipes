﻿#python

dir() will give you the list of in scope variables:
globals() will give you a dictionary of global variables with values
locals() will give you a dictionary of local variables with values
print(vars())
