#python 

All variables are local by default. From within a function the variable in the main procedure is viewable but cannot be changed. If the same name is used in the function than the procedure variable is hidden by the function variable (it is best not to use the same variable names in procedures and functions as it may cause confusion). Preceding the name of a variable by Global in a function links that variable to the same variable in the procedure and it may then be changed (this removes the 'encapsulation' of functions).

Global constants can be useful (often indicated by ALL CAPS).

A class, e.g. Global_vars, may also be used to store global variables.
