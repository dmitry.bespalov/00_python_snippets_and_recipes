"""
This code shows an example of using 'old' code, which we called  as imports
Mucisian and Dance (these are in a separate py file called 'external')
"""
# These are the 'old' methods we wish to use
# Musicians has a play() method, and Dancer has a dance() method

from external import Musician, Dancer

 
class Club: 
    """
    The new client.
    """
    
    def __init__(self, name): 
        self.name = name 
 
    def __str__(self): 
        return f'the club {self.name}' 
 
    def organize_event(self): 
        return 'hires an artist to perform for the people' 

        
class Adapter: 
    """
    Adapter class. 
    `obj` in __init__ is the object we want to adapt.
    `adapted_methods` is a dictionary that matches the method the client calls
    to the adapted method that needs to be called.
    Methods compatatible with the new client need no adaptaption.
    
    This adpater will mean that the client code continues to run the
    `organize_event`() on all objets. The adpater will take care of the 
    rest.
    """
    def __init__(self, obj, adapted_methods): 
        self.obj = obj 
        self.__dict__.update(adapted_methods) 
 
    def __str__(self): 
        return str(self.obj) 

        
def main(): 

    objects = [Club('Jazz Cafe'), Musician('Roy Ayers'), Dancer('Shane Sparks')]
    
    for obj in objects:
        # Look through available methods, and reference `paly` or `dance` to
        # `organize_event`.
        if hasattr(obj, 'play') or hasattr(obj, 'dance'):
            if hasattr(obj, 'play'):
                adapted_methods = dict(organize_event=obj.play)
            elif hasattr(obj, 'dance'):            
                adapted_methods = dict(organize_event=obj.dance)
                
            # referencing the adapted object here
            obj = Adapter(obj, adapted_methods)
            
        print(f'{obj} {obj.organize_event()}') 

  
if __name__ == "__main__": 
    main()