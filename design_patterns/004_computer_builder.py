
class Computer:
    def __init__(self, serial_number):
        self.serial = serial_number
        self.memory = None # in gigabytes
        self.hdd = None # in gigabytes
        self.gpu = None

    def __str__(self):
        """Returns info on computer `when print(computer)` is used """
        info = (f'Memory: {self.memory}GB',
                f'Hard Disk: {self.hdd}GB',
                f'Graphics Card: {self.gpu}')
        return '\n'.join(info)

# Builder
class ComputerBuilder:
    """Initialise computer and provide build methods, but does not build by 
    itslef.
    """
    def __init__(self):
        self.computer = Computer('AG23385193')

    def configure_memory(self, amount):
        self.computer.memory = amount

    def configure_hdd(self, amount):
        self.computer.hdd = amount

    def configure_gpu(self, gpu_model):
        self.computer.gpu = gpu_model

# Director
class HardwareEngineer:
    """Initialise builder.
    """
    def __init__(self):
        self.builder = None

    def construct_computer(self, memory, hdd, gpu):
        self.builder = ComputerBuilder()
        steps = (self.builder.configure_memory(memory),
                 self.builder.configure_hdd(hdd),
                 self.builder.configure_gpu(gpu))
        [step for step in steps]

    # HardwareEngineer.computrer getter
    @property
    def computer(self):
        return self.builder.computer

def main():
    # Initialise director (HardrwareEngineer)
    engineer = HardwareEngineer()
    # Instruct director to build computer with given specs
    engineer.construct_computer(hdd=500, 
                                memory=8, 
                                gpu='GeForce GTX 650 Ti')
    computer = engineer.computer
    print(computer)

if __name__ == '__main__':
    main()
