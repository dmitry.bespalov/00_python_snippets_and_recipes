#python
#list
#sort

from operator import itemgetter

scores=[["Moe",100],["Larry",500],["John",200],["Zack",500],["Adam",500]]

# Print scores

for entry in scores:
    name,score=entry
    print(name,"\t",score)

# Sort by descending score

scores.sort(key=itemgetter(1,0),reverse=True)
print("\nSorted:\n")
for entry in scores:
    name,score=entry
    print(name,"\t",score)

# Sort score in reversing order and name in ascending order 
# Sort is first by name, and this order is kept constant, or stable, 
# within the sorting by score

list1 = sorted(scores,key=itemgetter(0))
list1 = sorted(list1,key=itemgetter(1),reverse=True)
print("Sorted separately:\n")

for entry in list1:
    name,score=entry
    print(name,"\t",score)