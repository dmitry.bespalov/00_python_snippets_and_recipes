#python
#queue
#sort

# Finding min of tuples (x.y)

import heapq

tuple_list = [(3,4), (4,3), (1,5), (5,1), (2,6), (6,2)]

print('min x: ',min(tuple_list, key=lambda x: x[0]))
print('min y: ',min(tuple_list, key=lambda x: x[1]))
print()

# and if you want to sort the entire list, use sorted:

print ('Sorted by x:\n',sorted(tuple_list, key=lambda x: x[0]))
print ('Sorted by y:\n',sorted(tuple_list, key=lambda x: x[1]))
print ()

#Whereas if you want to use the three smallest values, use heapq.nsmallest:

print ('Smallest 3x:\n',heapq.nsmallest(3, tuple_list, key=lambda x: x[0]))
print ('Smallest 3y:\n',heapq.nsmallest(3, tuple_list, key=lambda x: x[1]))

Output:

min x:  (1, 5)
min y:  (5, 1)

Sorted by x:
 [(1, 5), (2, 6), (3, 4), (4, 3), (5, 1), (6, 2)]
Sorted by y:
 [(5, 1), (6, 2), (4, 3), (3, 4), (1, 5), (2, 6)]

Smallest 3x:
 [(1, 5), (2, 6), (3, 4)]
Smallest 3y:
 [(5, 1), (6, 2), (4, 3)]
