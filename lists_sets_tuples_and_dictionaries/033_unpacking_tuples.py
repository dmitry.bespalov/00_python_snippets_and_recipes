#python
#tuple

numbers = (1,2,3)
a,b,c=numbers

This would set a=1, b=2, c=3

A value with * takes all values (as a tuple) that are left over from the unpacking:

a,b,c*,d=[1,2,3,4,5,6,7,8,9]

c will = [3,4,5,6,7,8]

Values can also be swapped:
x,y=[1,2]
x,y=y,x
