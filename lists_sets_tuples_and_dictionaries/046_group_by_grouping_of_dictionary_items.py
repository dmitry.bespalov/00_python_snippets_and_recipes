# To subgroup a list of dictionary items by field

people = []
people.append({'born':1966, 'gender':'male', 'name':'Bob'})
people.append({'born':1966, 'gender':'female', 'name':'Anne'})
people.append({'born':1966, 'gener':'male', 'name':'Adam'})
people.append({'born':1970, 'gender':'male', 'name':'John'})
people.append({'born':1970, 'gender':'female', 'name':'Daisy'})
people.append({'born': 1968, 'gender':'male', 'name':'Steve'}) 

# import methods

from operator import itemgetter
from itertools import groupby

# First sort by required field
# Groupby only finds groups that a collected consecutively
people.sort(key=itemgetter('born'))

# Iterate through groups
for born, items in groupby(people, key=itemgetter('born')):
	print (born)
	for i in items:
		print(' ', i)
		
		

