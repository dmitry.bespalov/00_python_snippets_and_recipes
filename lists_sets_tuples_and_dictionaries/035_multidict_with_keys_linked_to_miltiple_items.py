#python
#dictionary
#multidict

"""
Building a dictionary with keys that are linked to multiple items in a list

"""

from collections import defaultdict

d=defaultdict(list)
d['a'].append(1)
d['a'].append(1)
d['b'].append(4)
d['a'].append(5)
print(d)
# output: defaultdict(<class 'list'>, {'b': [4], 'a': [1, 1, 5]})
print(d['a'])
# output: [1, 1, 5]

# Or a set can be used to automatically remove duplicates:
print('\nSets')
d=defaultdict(set)
d['a'].add(1)
d['a'].add(1)
d['b'].add(4)
d['a'].add(5)
print(d)   

# An odd behaviour of these dictionaries is that they create entries for keys
# accessed later that have no entry .......
print('\nDefault dictionary')
d={} # A regular dictionary
d.setdefault('a',[]).append(1)
d.setdefault('a',[]).append(2)
d.setdefault('b',[]).append(1)
d.setdefault('a',[]).append(4)
print(d)

