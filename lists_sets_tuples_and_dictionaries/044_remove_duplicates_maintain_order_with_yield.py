def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item
            seen.add(item)

a = [1, 2, 3, 1, 4, 6, 3, 2, 1, 10, 8]

x = list(dedupe(a))
print (x)