
my_dict = {'canada goose':10,'sparrow':16}
my_list = ['canada goose','sparrow']
my_nested_list =  [['canada goose',10],
                   ['sparrow', 46]]
my_tuples = ('canada goose','sparrow')
my_nested_tuples =  (('canada goose',10),
                   ('sparrow', 46))
my_set = {'sparrow','canada goose'}

# In does not work in nested lists and tuples for single items
print('sparrow' in my_dict)
print('sparrow' in my_list)
print('sparrow' in my_set)
print('sparrow' in my_tuples)
print('sparrow' in my_nested_list)
print('sparrow' in my_nested_tuples)