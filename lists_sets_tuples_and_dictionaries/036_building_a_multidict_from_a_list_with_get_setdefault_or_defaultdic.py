#python
#multidict
#dictionary

setdefault is a bit more efficient than get because it only sets the key when necessary. setdefault looks for the value of "key" in the dictionary, if not available, enters "[]" into the dict, returns a reference to either the value or "[]" and appends "val" to that reference.

data = [('a', 1), ('b', 1), ('b', 2)]

d1 = {}
d2 = {}

for key, val in data:
    # variant 1)
    d1[key] = d1.get(key, []) + [val] # rewrites dictionary item by concatenation
    # variant 2)
    d2.setdefault(key, []).append(val) # adds to existing lsit

This can also be done with defaultdic:

from collections import defaultdict

data = [('a', 1), ('b', 1), ('b', 2)]

d3 = defaultdict(list)

for k, v in data:
    d3[k].append(v)

Or the long way:

data = [('a', 1), ('b', 1), ('b', 2)]

d = {}
for key, val in data:
    if key not in d:
       d[key]=[]
    d[key].append(val)
