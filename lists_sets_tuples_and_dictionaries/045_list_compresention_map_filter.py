my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# APPLY A FUNCTION TO ALL MEMBERS OF A LIST

# Define lambda function and map to list (normal functions may also be mapped)
sq = lambda x: x**2
print (list(map(sq, my_list)))

# Apply lamba diractly to list
print (list(map(lambda x: x**2, my_list)))

# Using list comprehension
print([x**2 for x in my_list])

# Combining list comprehension (also using if in list comprehension)
print([sq(x) for x in my_list if x%2 == 0])

# filter applies a boolean function to a list (simple lambda funciton used here)
# find even numbers
print(list(filter(lambda x: x%2==0, my_list)))

# Or by list comprehesion
print([x for x in my_list if x%2 ==0])
