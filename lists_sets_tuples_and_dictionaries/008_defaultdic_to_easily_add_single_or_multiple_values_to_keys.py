from collections import defaultdict

# defaultdic allows multiple values to be easily added to a dictionary key.
# Those values may be stored as a list or a set (the latter will eliminate
# # duplicates). If the key does not exit it will be created.

# To store multiple values as a list

d = defaultdict(list)

d['a'].append(1)
d['b'].append(2)
d['a'].append(3)
d['a'].append(3)

print (d['a'])

d = defaultdict(set)

d['a'].add(1)
d['b'].add(2)
d['a'].add(3)
d['a'].add(3)

print (d['a'])