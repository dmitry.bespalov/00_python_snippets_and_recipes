#python
#pygame

# Elipse takes a rectagngle that the elipse will fit into. 

# The following code draws an elipse from the top left to the mouse position.

import pygame
from pygame.locals import *
from sys import exit 

from random import *

pygame.init()
screen = pygame.display.set_mode((640, 480), 0, 32)

while True:

    for event in pygame.event.get():
        if event.type == QUIT:
          pygame.quit()
          exit()

    x, y = pygame.mouse.get_pos() 
    screen.fill((255,255,255)) 
    width=0
    pygame.draw.ellipse(screen, (0,255,0), (0,0,x,y),width)

    pygame.display.update()


