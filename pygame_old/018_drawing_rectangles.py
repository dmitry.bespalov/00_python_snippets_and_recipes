#python
#pygame

Rectangle
---------

Simple rectangle:

my_rect1=(x,y,width,height)

also:

my_recet2=((x,y),(width,height))


Rectangle object class:
-----------------------

See https://www.pygame.org/docs/ref/rect.html

Rect object class provides convenient rectangle methods


from pygame import rect (not needed if from pygame import * is used)

my_rect3=Rect(x,y,width,height)

or:

my_recet4=Rect((x,y),(width,height))

pygame.Rect.copy 	— 	copy the rectangle
pygame.Rect.move 	— 	moves the rectangle
pygame.Rect.move_ip 	— 	moves the rectangle, in place
pygame.Rect.inflate 	— 	grow or shrink the rectangle size
pygame.Rect.inflate_ip 	— 	grow or shrink the rectangle size, in place
pygame.Rect.clamp 	— 	moves the rectangle inside another
pygame.Rect.clamp_ip 	— 	moves the rectangle inside another, in place
pygame.Rect.clip 	— 	crops a rectangle inside another
pygame.Rect.union 	— 	joins two rectangles into one
pygame.Rect.union_ip 	— 	joins two rectangles into one, in place
pygame.Rect.unionall 	— 	the union of many rectangles
pygame.Rect.unionall_ip 	— 	the union of many rectangles, in place
pygame.Rect.fit 	— 	resize and move a rectangle with aspect ratio
pygame.Rect.normalize 	— 	correct negative sizes
pygame.Rect.contains 	— 	test if one rectangle is inside another
pygame.Rect.collidepoint 	— 	test if a point is inside a rectangle
pygame.Rect.colliderect 	— 	test if two rectangles overlap
pygame.Rect.collidelist 	— 	test if one rectangle in a list intersects
pygame.Rect.collidelistall 	— 	test if all rectangles in a list intersect
pygame.Rect.collidedict 	— 	test if one rectangle in a dictionary intersects
pygame.Rect.collidedictall 	— 	test if all rectangles in a dictionary intersect

The Rect object has several virtual attributes which can be used to move and align the Rect:

x,y
top, left, bottom, right
topleft, bottomleft, topright, bottomright
midtop, midleft, midbottom, midright
center, centerx, centery
size, width, height
w,h

All of these attributes can be assigned to, e.g.:

rect1.right = 10
rect2.center = (20,30)




