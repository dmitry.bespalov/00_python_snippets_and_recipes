import math

# Numpy is often a good method for handling vectors.
# Beware lists and tuples as functions like + conctentate rather than add

class Vector2:
    
    def __init__(self, x=0, y=0):        
        self.x = x
        self.y = y

        if hasattr(x, "__getitem__"):
            x, y = x
            self._v = [float(x), float(y)]
        else:
            self._v = [float(x), float(y)]
        
    def __str__(self):
        return "(%s, %s)"%(self.x, self.y)
        
    def from_points(P1, P2): 
        # create a vector (e.g. travel) from two vectors (e.g. points)        
        return Vector2( P2[0] - P1[0], P2[1] - P1[1] )
    
    def get_magnitude(self):  
        # return pythagorean distance between two vectors      
        return math.sqrt( self.x**2 + self.y**2 )
        
    def normalize(self):
        # divide vector by magnitude (e.g. gives direction without distance) 
        magnitude = self.get_magnitude()
        self.x /= magnitude
        self.y /= magnitude
        
    def __add__(self, rhs):
        # __add__ defines method to use when encountering '+'   
        return Vector2(self.x + rhs.x, self.y + rhs.y)
        
    def __sub__(self, rhs):
        # __sub__ defines method to use when encountering '-' 
        return Vector2(self.x - rhs.x, self.y - rhs.y)
        
    def __neg__(self):
        # tuen a vector through 180 degress
        return Vector2(-self.x, -self.y)
        
    def __mul__(self, scalar):
        # __mul__ defines method to use when encountering '+*
        # this method multiplies by a scalar
        return Vector2(self.x * scalar, self.y * scalar)
        
    def __truediv__(self, scalar):
        # __truediv__ defines method to use when encountering '+'
        # this method divides by a scalar
        # useful, for example, for calculating time steps along a larger vector
        return Vector2(self.x / scalar, self.y / scalar)

    def __getitem__(self, index):
        # get an index value
        return self._v[index]

    def __setitem__(self, index, value):
        # set an index value
        self._v[index] = 1.0 * value

