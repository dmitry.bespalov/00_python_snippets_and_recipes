#python
#numpy
#where

Normal conditional testing of an array returns a boolean.
Where returns index of values that meet condition.

x=np.array([0,12,5,20])

x>10
Out[32]: array([False,  True, False,  True], dtype=bool)

np.where(x>10)
Out[33]: (array([1, 3], dtype=int64),)