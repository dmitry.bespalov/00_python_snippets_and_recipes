#python
#numpy
#argmax
#argmin

The following shows how argmax is used to return the index of the maximum value.
argmin is used in a similar way.


>>> a = np.arange(6).reshape(2,3)

>>> a
array([[0, 1, 2],
       [3, 4, 5]])

>>> np.argmax(a)
5

>>> np.argmax(a, axis=0) # returns row index of each column max
array([1, 1, 1])

>>> np.argmax(a, axis=1) # retruns colum inde of each row max
array([2, 2])


The following example shows that only the first equal maximum value is returned

>>> b = np.arange(6)

>>> b[1] = 5

>>> b
array([0, 5, 2, 3, 4, 5])
>>> np.argmax(b) # Only the first occurrence is returned.
1
