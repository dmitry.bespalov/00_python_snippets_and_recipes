#python
#numpy
#slice

Slicing

Slices are references to memory of the original array. 
Changing a value of a slice changes the value of the original array. 
Use the .copy() command to create an independent copy.

x[:-1] creates slices of x from start to end -1
x[-2:] creates slice of last two elements
x[-4:-2] creates slices from 4 from the end to 3 from the end
x[::2] every other element
x[1::2] every other element using odd number indexes
x[::-1] reverses the array

