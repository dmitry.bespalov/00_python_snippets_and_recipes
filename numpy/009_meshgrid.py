#python
#numpy
#meshgrid

# creating a a 2d array from two 1 d arrays
# points is a 100 point 1d array
# meshgrid uses points for both axes and creates a 1000 x 1000 matrix
# xs, ys and z are all 1000 x 1000 arrays

import numpy as np
import pprint
import random
import matplotlib.pyplot as plt


pp=pprint.pprint # make pp

points=np.arange(-5,5,0.01) # 1000 equally spaced points

xs,ys=np.meshgrid(points, points) # produces two arrays of x,y

pp (xs)

z=np.sqrt(xs**2+ys**2) # function of two matrices

pp (z)

plt.imshow(z,cmap=plt.cm.gray);plt.colorbar()

