#python
#numpy
#copy

Using = creates a reference rather than a copy

a=np.arange(5)
b=a
b[1]=200
In [17]: a
Out[17]: array([ 0, 200, 2, 3, 4])

Can create a copy e.g. by using 
b=a.copy()

Now B can be changed without altering A
