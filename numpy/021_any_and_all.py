#python
#numpy
#any
#all

x=np.array([1,2,3,4,5,6])
y=np.array([1,2,3,4,5,6])
z=np.array([1,2,3,4,5,7])

np.all(x==y)
Out[5]: True

np.all(x==z)
Out[6]: False

np.any(x==z)
Out[7]: True