#python
#numpy
#transpose
#reshape

import numpy as np
import pprint
import random

pp = pprint.pprint  # make pp

## Reshaping arrays

arr = np.arange(32)
pp(arr)

arr2 = arr.reshape((8, 4))
print()
pp(arr2)

## Transpose array

arr3 = arr2.T
# arr3=np.transpose(arr2) # alternative transpose syntax
print()
pp(arr3)

## reshaping and transposing in more than two dimensions

arr4 = np.arange(16).reshape((2, 2, 4))
print()
pp(arr4)

# transposing n more than two dimensions is mind-bending

arr5 = arr4.transpose((1, 0, 2))
print()
pp(arr5)

# transposing can swap any axes
arr6 = arr5.swapaxes(1, 2)
print()
pp(arr6)
