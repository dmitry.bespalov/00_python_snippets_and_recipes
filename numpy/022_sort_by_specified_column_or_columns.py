#python
#numpy
#pandas
#sort


argsort method
==============

# Applying index to sort array by given column

x=np.array([[1.2,5.6],
            [10,0.3],
            [55,0],
            [2.2,7.7],
            [8,3]])
y=x[:,1] # second column of x used for sorting
sort_order=np.argsort(y)
print('Original matrix, matrix sorted by second column')
print(x)
print()
print(x[sort_order,:])

# Note order may be reverse using sort_order=sort_order[::-1]




pandas method
=============

# Pandas is used to maintain order of columns while sorting by one (or more) column.

import numpy as np
import pandas as pd

# Create arrays
DummyArray=np.zeros((10,2)) # Create 2*10 NumPy Array
DummyArray[:,0]=np.arange(10) # Fill column zero with range 0-9
DummyArray[:,1]=np.random.rand(10)*100 # Fill column 1 with an array of 10 random numbers between zero and 100

# Copy to Pandas
PandasDF=pd.DataFrame(DummyArray) # Create Pandas array from Numpy Array
PandasDF.columns=['ID','Score'] # Add columns names to Pandas Array

# The following creates a sorted dataframe, sorted by descending score and then subsorted if necessary by ascending ID
SortedByScore=PandasDF.sort(['Score','ID'],ascending=[0,1])

# If necessary the Pandas array can be converted back into a NumPy array (no headers)
NumpySorted=np.array(SortedByScore)
print(PandasDF)
print(NumpySorted)
