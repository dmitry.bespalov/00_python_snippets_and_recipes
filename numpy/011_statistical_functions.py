#python
#numpy


"""
	* average(), mean()
	* bincount(), histogram()
	* corrcoef() ; Pearson product-moment correlation coefficients as array
	* cov() ; covariance matrix
	* max(), min(), ptp() ; ptp is range
	* median()
	* std(), var()
	* sum()
"""

# Example....

import numpy as np
import pprint
import random


pp=pprint.pprint # make pp

arr=np.random.randn(5,3) # normally distrinuted data
print(arr.mean())

pp(arr)

print("Mean",arr.mean())
print ("Mean",np.mean(arr)) # the numpy function
print("Median",np.median(arr))

# Functions may be restricted, e.g.

print("Mean of axis 1",arr.mean(axis=1))
print("Median of axis 1",np.median(arr,axis=1))

# Boolian methods assume True=1 and False=0, and may be summed

arr=np.random.randn(100)
print((arr>0).sum())


