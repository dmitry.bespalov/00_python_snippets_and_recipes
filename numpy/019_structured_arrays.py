#python
#numpy
#structured arrays

Pandas are usually preferred, but structured arrays may be faster.

4.2. Creating Structured Arrays
A structured array in numpy is an array of records. 
Each record can contain one or more items which can be of different types. 
For example, a record showing student�s performance on an exam can contain information on 
(first name, last name, score on the exam). The first two elements in the record are strings 
and the last element is a float.

A structured array can be created by using the dtype object to define the fields of a record. 
These fields can be defined in several ways. We specify two ways here. 

To learn other ways see here: http://docs.scipy.org/doc/numpy/user/basics.rec.html

Suppose we want to create an empty array in which we can store 5 records, each record containing 
a student�s first name, last name and the score on the final exam. One way to do this is to specify 
dtype object as a list of (name, data format) tuples.

import numpy as np
studentgrades = np.zeros(5, dtype = [('First Name','a20'),
                                         ('Last Name', 'a20'),
                                         ('Score', 'f4')]
                           )
Another way to create this array is to specify the dtype as a dictionary with two keys 
�names� and �formats� and values are field names and the data formats for each field name:

import numpy as np

studentgrades = np.zeros(5, dtype = {'names': ['First Name', 'Last Name', 'Score'],
                                     'formats': ['a20', 'a20', 'f4']} )
Let us add records to this array.
studentgrades[0] = ('Dave', 'Thomas', 89.4)
studentgrades[1] = ('Tasha', 'Han', 76.6)
studentgrades[2] = ('Cool', 'Python', 100)
studentgrades[3] = ('Stack', 'Overflow', 95.32)
studentgrades[4] = ('Py', 'Py', 75)


Printing the array using print studentgrades we get

[('Dave', 'Thomas', 89.400001525878906)
 ('Tasha', 'Han', 76.599998474121094) ('Cool', 'Python', 100.0)
 ('Stack', 'Overflow', 95.319999694824219) ('Py', 'Py', 75.0)]

The beauty of structured arrays is that we can access data by field names. For example:

firstnames = studentgrades['First Name']
print firstnames
will return

['Dave' 'Tasha' 'Cool' 'Stack' 'Py']

All scores can be obtained by
print studentgrades['Score']

The above gives
[  89.40000153   76.59999847  100.           95.31999969   75.        ]

To learn more on structured arrays, visit http://docs.scipy.org/doc/numpy/user/basics.rec.html