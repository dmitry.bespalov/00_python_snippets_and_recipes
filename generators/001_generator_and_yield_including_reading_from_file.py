#python
#generator
#yield
#file

### This will keep yielding increments of 0.1

def my_generator():
    x=0
    while True:
        yield x
        x+=0.1

gen=my_generator()
for i in gen:
    print('%.2f' %(i))
    if i>=10:
        break


T# his will yield 0.5 fractions in a defined range

# Generator function
def make_gen():
     for x in range(0,100):
          yield x /2

gen = make_gen()
for i in gen:
     print (i)
     if i>25:
          break
          print("Complete")


# Example of using generator to go through file:

def search(keyword, filename):
    print('generator started')
    f = open(filename, 'r')
    # Looping through the file line by line
    for line in f:
        if keyword in line:
            # If keyword found, return it
            yield line
    f.close()
