# Generators

# Define generator
def square_numbers(num):
    for i in num:
        yield i**2

# Create generator object
gen_nums = square_numbers(range(1,7))

# Iterate through generator using `next`
print ('Iterate using next')
for i in range(6):
    print (next(gen_nums))

# Re-create generator object
gen_nums = square_numbers(range(1,7))

# Iterate through generator using simple loop
print ('\nIterate using simple loop')
for num in gen_nums:
    print(num)

# Create generator object using list comprehension
# Use of () instead of [] creates generator
gen_nums = (x**2 for x in range(1,7))

# Show generator object has been created
print (gen_nums)

# Print from generator
for num in gen_nums:
    print(num)

# Create list from generator (but this uses more memory)
gen_nums = (x**2 for x in range(1,7))
my_nums = list(gen_nums)
print ('List created from generator')
print (my_nums)


