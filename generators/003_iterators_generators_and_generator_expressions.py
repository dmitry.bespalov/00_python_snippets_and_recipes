#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of generator object
"""

def check_prime(number):
    for divisor in range(2, int(number ** 0.5) + 1):
        if number % divisor == 0:
            return False
    return True

# An iterator object

class Primes:
    """
    Pass maximum value to Primes.
    Every iteration of the Primes object calls __next__ to generate the next
    prime number. Iterators can only be iterated over once. If you try to
    iterate over primes again, no value will be returned. It will behave like
    an empty list. A new instance can be made to start again.
    """
    def __init__(self, max):
        self.max = max
        self.number = 1
    def __iter__(self):
        return self
    def __next__(self):
        self.number += 1
        if self.number >= self.max:
            raise StopIteration
        elif check_prime(self.number):
            return self.number
        else:
            return self.__next__()

print ('Iterator')
primes = Primes(100)
print(primes)
for x in primes:
    print(x, end = ' ')
print()    
    
# A generator: simplifies iterator objects

print ('\nGenerator')    
def Primes(max):
    number = 1
    while number < max:
        number += 1
        if check_prime(number):
            yield number

primes = Primes(100)
print(primes)
for x in primes:
    print(x, end = ' ')    
print()

# A generator expression (like list comprhension but with () instead of [])
    
print ('\nGenerator expression')    
primes = (i for i in range(2, 100) if check_prime(i))
print(primes)
for x in primes:
    print(x, end  = ' ')