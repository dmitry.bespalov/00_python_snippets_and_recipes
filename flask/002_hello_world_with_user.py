"""
Open development server at http://127.0.0.1:5000/
"""


from flask import Flask

# Set up instance of Flask object
app = Flask(__name__) # root path of Python code; __name__ is good default

# define route for incoming web traffic 
# can also use name/integer to custom-route, e.g. @app.route('user/<name>')

@app.route('/')
def index():
    return '<h1>Hello World</h1>'

@app.route('/user/<name>')
def user(name):
    return '<h1>Hello, %s!</h1>' % name


# Start up a development server if Python code run directly
if __name__ == '__main__':
    app.run(debug = True)

