"""
Open development server at http://127.0.0.1:5000/
"""

# request is an object that contains the HTTP request sent by the client
# It is part of the application 'context'

from flask import Flask
from flask import redirect

# Set up instance of Flask object
app = Flask(__name__) # root path of Python code; __name__ is good default

@app.route('/')
def index():
    return redirect('http://bbc.co.uk')


# Start up a development server if Python code run directly
if __name__ == '__main__':
    app.run(debug = True)

