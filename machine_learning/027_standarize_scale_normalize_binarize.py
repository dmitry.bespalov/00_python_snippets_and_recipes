# import ipdb; ipdb.set_trace()

# See http://scikit-learn.org/stable/modules/preprocessing.html

import numpy as np
from sklearn import preprocessing

data = np.array([[3,-1.5,2,-5.4],
                 [0,4,-0.3,2.1],
                 [1,3.3,-1.9,-4.3]])

# standardise data to mu=0 and std dev=1
data_standardised = preprocessing.scale(data)
print('\nMean =',data_standardised.mean(axis = 0))
print('Std Dev =', data_standardised.std(axis = 0))

# scale data so each feature ranges between 0 and 1 (or other range)
data_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
data_scaled = data_scaler.fit_transform(data)
print('\nMin max scaled data =',data_scaled)

# Normalise data so all features on same scale
# One common method is to adjust features so each feature sums to 1
# l1 reduces mean diff; l2 reduces squared diff of values
data_normalised = preprocessing.normalize(data, norm='l1')
print ('\nL1 normalised data =',data_normalised)

# binarize data; convert to boolean based on cut-off
data_binarized = preprocessing.Binarizer(threshold=1.4).transform(data)
print('\nBinarized data =',data_binarized)
