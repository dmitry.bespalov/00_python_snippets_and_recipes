#python
#machine learning
#sklearn
#matplotlib
#random forests


Note: Random forests report average impurity decrease computed from all features. 
No assumptions are made about how linearly separable the data are. 
But highly correlated data may end with one as high importance and the other as low importance)


from sklearn.ensemble import RandomForestClassifier
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df_wine=pd.read_csv('data/wine.csv',header=None)
df_wine.columns=['Class label','Alcohol','Malic acid','Ash','Alcalinity of ash',
                 'Magnesium','Total phenols','Falvenoids','Non flavenoid phenols',
                 'Proanthpcyanins','Color intensity','Hue','OD280/OD315 of diluted wines',
                 'Proline']

feat_labels = df_wine.columns[1:]

forest = RandomForestClassifier(n_estimators=10000,
                                random_state=0,
                                n_jobs=-1)
from sklearn.cross_validation import train_test_split
X,y=df_wine.iloc[:,1:].values,df_wine.iloc[:,0].values # X=data, y=label
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.3,random_state=0)


forest.fit(X_train, y_train)
importances = forest.feature_importances_

indices = np.argsort(importances)[::-1]

for f in range(X_train.shape[1]):
    print("%2d) %-*s %f" % (f + 1, 30,
                            feat_labels[indices[f]],
                            importances[indices[f]]))

plt.title('Feature Importances')
plt.bar(range(X_train.shape[1]),
        importances[indices],
        color='lightblue',
        align='center')

plt.xticks(range(X_train.shape[1]),
           feat_labels[indices], rotation=90)
plt.xlim([-1, X_train.shape[1]])
plt.tight_layout()
#plt.savefig('./random_forest.png', dpi=300)
plt.show()

OUTPUT

 1) Color intensity                0.182483
 2) Proline                        0.158610
 3) Falvenoids                     0.150948
 4) OD280/OD315 of diluted wines   0.131987
 5) Alcohol                        0.106589
 6) Hue                            0.078243
 7) Total phenols                  0.060718
 8) Alcalinity of ash              0.032033
 9) Malic acid                     0.025400
10) Proanthpcyanins                0.022351
11) Magnesium                      0.022078
12) Non flavenoid phenols          0.014645
13) Ash                            0.013916

?

