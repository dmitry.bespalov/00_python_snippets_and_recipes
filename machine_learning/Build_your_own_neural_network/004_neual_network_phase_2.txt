#python
#machine learning
#neural network

"""Three layer neural network for handwriting digit recognition"""


import numpy as np
import datetime
import scipy.special # includes sigmoid function
import matplotlib as plt

# Start
print ()
print (datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'),'Start\n')

class Neural_Network:
    # Initialise

    def __init__(self,input_nodes,hidden_nodes,output_nodes,learning_rate):
        self.inodes=input_nodes
        self.hnodes=hidden_nodes
        self.onodes=output_nodes
        self.learn_rate=learning_rate
        self.weights_ih=np.random.rand(self.hnodes,self.inodes)-0.5 # rows are the next layer of nodes
        self.weights_ho=np.random.rand(self.onodes,self.hnodes)-0.5

    # Define activation function
    def sigmoid_activation(self,x):
        y=scipy.special.expit(x)
        return (y)
        
    # Query (convert input to outputs)
    def query(self,inputs):
        hidden_inputs=np.dot(self.weights_ih,inputs) # Calculate inputs into hidden layer
        hidden_outputs=self.sigmoid_activation(hidden_inputs) # Calculate signal from hidden layer
        final_inputs=np.dot(self.weights_ho,hidden_outputs) # Calculate signal into final layer
        final_outputs=self.sigmoid_activation(final_inputs) # Calculate signal out of final layer
        return (final_outputs)
        
    def train(self,input_array,target_array):
        inputs=np.array(input_array,ndmin=2).T
        targets=np.array(target_array,ndmin=2).T
        hidden_inputs=np.dot(self.weights_ih,inputs) # Calculate inputs into hidden layer
        hidden_outputs=self.sigmoid_activation(hidden_inputs) # Calculate signal from hidden layer
        final_inputs=np.dot(self.weights_ho,hidden_outputs) # Calculate signal into final layer
        final_outputs=self.sigmoid_activation(final_inputs) # Calculate signal out of final layer
        # Calculate error        
        output_errors=targets-final_outputs          
        # Calculating weights:
        # Errors (hidden) = weights of hidden output * error of output
        # Hidden layer error is the output_errors, split by weights, recombined at hidden nodes
        hidden_errors=np.dot(self.weights_ho.T,output_errors)
        # Update the weights for the links between the hidden and output layers
        self.weights_ho+=self.learn_rate*np.dot((output_errors*final_outputs*(1.0-final_outputs)),np.transpose(hidden_outputs))
        self.weights_ih+=self.learn_rate*np.dot((hidden_errors*hidden_outputs*(1.0-hidden_outputs)),np.transpose(inputs))
    


# Load training and test data
training_set=np.loadtxt('mnist/mnist_train.csv',delimiter=",")
test_set=np.loadtxt('mnist/mnist_test.csv',delimiter=",")

# Initialise variables
input_nodes=784
hidden_nodes=200
output_nodes=10
learning_rate=0.3

# Initialise network
n=Neural_Network(input_nodes,hidden_nodes,output_nodes,learning_rate)

# Train
for training_run in range (5): # training set can be iterated through serval times, with refined learning
    print (datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'),'Training run:'
    ,training_run+1,', Learning rate:',n.learn_rate)
    for record in training_set:
        label=int(record[0])
        pixel_values=record[1:]
        inputs=(pixel_values/255*0.99)+0.01 # normalise 0.01 to 1.00
        targets=np.zeros(output_nodes)+0.01 # Set all targets to low
        targets[label]=1
        n.train(inputs,targets)
    n.learn_rate=n.learn_rate*0.5 # Learning rate is slower with each run through

# Test
print ()
print (datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'),'Begin test')
print ()
scorecard=[]
for record in test_set:
    label=int(record[0])
    pixel_values=record[1:]
    inputs=(pixel_values/255*0.99)+0.01 # normalise 0.01 to 1.00
    outputs=n.query(inputs)
    predicted=np.argmax(outputs)
    if predicted==label:
        scorecard.append(1)
    else:
        scorecard.append(0)
print('Average accuracy (%):',np.average(scorecard)*100)
    
# Show example
test_id=6
record=test_set[test_id]
label=int(record[0])
pixel_values=record[1:]
inputs=(pixel_values/255*0.99)+0.01 # normalise 0.01 to 1.00
outputs=n.query(inputs)
predicted=np.argmax(outputs)
image_array=pixel_values.reshape((28,28))
plt.pyplot.imshow(image_array,cmap='Greys', interpolation='None')
print('\nActual:',label,' Predicted:',predicted)
print()

# End
print (datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'),'End')