#python
#machine learning
#sklearn
#feature scaling



import numpy as np
import pandas as pd

# Original data load
#df_wine=pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data',header=None)
#df_wine.to_csv('wine.csv')
#remove generated index and header columns from csv manually

df_wine=pd.read_csv('wine.csv',header=None)
df_wine.columns=['Class label','Alcohol','Malic acid','Ash','Alcalinity of ash',
                 'Magnesium','Total phenols','Falvenoids','Non flavenoid phenols',
                 'Proanthpcyanins','Color intensity','Hue','OD280/OD315 of diluted wines',
                 'Proline']
print(np.unique(df_wine['Class label']))

# Split the data set into training and test

from sklearn.cross_validation import train_test_split
X,y=df_wine.iloc[:,1:].values,df_wine.iloc[:,0].values # X=data, y=label
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.3,random_state=0)

# Feature scaling

# Normalisation
from sklearn.preprocessing import MinMaxScaler
mms=MinMaxScaler()
X_train_norm=mms.fit_transform(X_train)
X_test_norm=mms.fit_transform(X_test)

# Standardisation (mean=0, SD=1)
from sklearn.preprocessing import StandardScaler
stdsc=StandardScaler()
X_train_norm=stdsc.fit_transform(X_train)
X_test_norm=stdsc.fit_transform(X_test)
