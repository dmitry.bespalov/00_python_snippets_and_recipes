#python
#machine learning
#sklearn
#impute
#missing
#drop


import pandas as pd
import numpy as np
from io import StringIO # Allows reading of string as if it were coming from hard drive
csv_data='''A,B,C,D
1.0,2.0,3.0,4.0
5.0,6.0,,8.0
10.0,11.0,12.0,'''
df=pd.read_csv(StringIO(csv_data))
print(df)

# Show count of missing values in each column
print('\nShow count of missing values')
print(df.isnull().sum())

# To convert pandas to numpy
np_array=df.values
print('\nConvert to numpy array')
print(np_array)

# To drop rows  with missing values
print('\nDrop rows with missing values')
print (df.dropna())

# To drop coluns with missing values
print('\nDrop rows with missing values')
print (df.dropna(axis=1))

# Only drop rows where all values are missing
print('\nDrop rows with all missing values')
print (df.dropna(how='all'))

# Only drop rows with at least 4 missing values
print('\nDrop rows with at least 4 non-missing values')
print (df.dropna(thresh=4))

# Drop rows with missing values in specific column
print('\nDrop rows with missing values in column C')
print (df.dropna(subset=['C']))

### Impute missing data ####
from sklearn.preprocessing import Imputer

# Use mean of columns to impute data
imr=Imputer(missing_values='NaN',strategy='mean',axis=0)
imr=imr.fit(df)
imputed_data=imr.transform(df.values)
print('\nImputed data using mean of columns')
print(imputed_data)

# Use mean of rows to impute data
imr=Imputer(missing_values='NaN',strategy='mean',axis=1)
imr=imr.fit(df)
imputed_data=imr.transform(df.values)
print('\nImputed data using mean of rows')
print(imputed_data)

# Use median of columns to impute data
imr=Imputer(missing_values='NaN',strategy='median',axis=0)
imr=imr.fit(df)
imputed_data=imr.transform(df.values)
print('\nImputed data using median of columns')
print(imputed_data)

# 'most_frequent' uses mode to replace missing values (most useful for categorical values)
