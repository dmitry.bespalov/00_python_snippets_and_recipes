import pandas as pd

data = pd.read_csv('data/IMDb.csv')

#%% Using beautifulsoup to clean (e.g. remove markup)
# pip install BeautifulSoup4 
from bs4 import BeautifulSoup
import re

def preprocessor(text):
    # Initialise (lxml is a parser; this can be removed and Python will choose)
    bs = BeautifulSoup(text, "lxml")
    text = bs.get_text()
    
    # keep only letters (keeps spaces, but removes punctuation, another
    # possibility is to replace all non-letters with space)
    letters_only = re.sub("[^a-zA-Z ]", "", text )
    # convert to lowercase
    text = letters_only.lower()
    # optoinal removal of stopwords
    text = remove_stop_words(text)
    return text


def remove_stop_words(text):
    # the following command needs to be run in terminal once to set up pc
    # nltk.download('stopwords')
    words = text.split()
    from nltk.corpus import stopwords
    stops = set(stopwords.words("english")) # using a set is faster than a list               
    from nltk.corpus import stopwords
    meaningful_words = [w for w in words if not w in stops]
    # print stopwords.words("english")
    return ( " ".join(meaningful_words))
    
data['review_souped'] = data['review'].apply(preprocessor)