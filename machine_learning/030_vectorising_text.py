import numpy as np

#%% Simple bag of words

from sklearn.feature_extraction.text import CountVectorizer

count = CountVectorizer()
docs = np.array([
        'The sun is shining',
        'The weather is sweet',
        'The sun is shining and the weather is sweet'])

# Create bag of words (sparse)
bag = count.fit_transform(docs)

# Print the vocuabular dictionary. Values are column index for word bag
print('\nBag dictionary')
print(count.vocabulary_)

# Print word bag as array. # These are also called raw term frequencies: 
# tf(t,d) - the number of times a term t occurs in a document d
print('\nRaw term frequencies')
print (bag.toarray())

# Note here we are counting single words (1-gram or unigram'). It is also
# to count the number of times sequence of n-words occurs (n-gram). n-gram of
# 3-4 is good for spam.

#%% Tfidf Transformation (term frequency, inverse document frequency)
# Tfidf adjusts for how common a word is across the original documents. The 
# more common a word is the less useful it is for discriminattion

from sklearn.feature_extraction.text import TfidfTransformer
tfidf = TfidfTransformer()
np.set_printoptions(precision=2)
print('\nTfidf Transform')
print (tfidf.fit_transform(count.fit_transform(docs)).toarray())