﻿#python
#stats
#matplotlib
#3d
#wireframe
#contour

import numpy as np
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

'''Generation of 3D plots'''
    
# imports specific to the plots in this example
from matplotlib import cm   # colormaps

# This module is required for 3D plots!
from mpl_toolkits.mplot3d import Axes3D

# Twice as wide as it is tall.
fig = plt.figure(figsize=plt.figaspect(0.5))


#---- First subplot
# Generate the data
X = np.arange(-5, 5, 0.1)
Y = np.arange(-5, 5, 0.1)
X, Y = np.meshgrid(X, Y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)

# Note the definition of "projection", required for 3D  plots
#plt.style.use('ggplot')

ax = fig.add_subplot(1, 2, 1, projection='3d')
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.GnBu,
        linewidth=0, antialiased=False)
#surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.viridis_r,
        #linewidth=0, antialiased=False)
ax.set_zlim3d(-1.01, 1.01)

fig.colorbar(surf, shrink=0.5, aspect=10)

#---- Second subplot
# Get some 3d test-data
from mpl_toolkits.mplot3d.axes3d import get_test_data

ax = fig.add_subplot(1, 2, 2, projection='3d')
X, Y, Z = get_test_data(0.05)
ax.plot_wireframe(X, Y, Z, rstride=10, cstride=10)
plt.show()
