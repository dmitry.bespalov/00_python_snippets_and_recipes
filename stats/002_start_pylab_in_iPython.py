﻿%pylab inline

Brings NumPy and atPlotLib into workspace.#

In Jupyter:
%mapltotlib inline # for inline plots
%matplotlib qt4 # for external graphics window