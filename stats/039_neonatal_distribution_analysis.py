import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.ticker as ticker
import statsmodels.api as sm

data=pd.read_csv('los_for_distribution_analysis.csv')
combined = pd.concat([data['actual-model_l1'],
                     data['actual-model_l2'],
                     data['actual-model_l3'],
                     data['actual-model_l4']])
x = combined[~combined.isnull()]

mean = x.mean()
stdev = x.std()

fig = plt.figure(figsize=(8,4),dpi=300)

## Plot distribution
ax1 = fig.add_subplot(121) # Grid of 1 x 32 this is suplot 1
num_bins = 50
n, bins, patches = ax1.hist(x, num_bins, normed=1,color='blue')
# add a 'best fit' line
y = mlab.normpdf(bins, mean, stdev)
ax1.plot(bins, y, 'r--')
ax1.set_xlabel('ln(los) - ln(model los)')
ax1.set_ylabel('Probability density')
ax1.set_xlim(-4.2,4.2)
ax1.set_ylim(0,0.52)
ax1.xaxis.set_major_locator(ticker.MultipleLocator(1))

plt.text(-3.9,0.45,'Mean = -0.03\nStDev = 1.02')


data = x
ax2 = fig.add_subplot(122)
sm.graphics.qqplot(x, line='45',ax=ax2)
ax2.set_xlim(-4.2,4.2)
ax2.set_ylim(-4.2,4.2)
ax2.xaxis.set_major_locator(ticker.MultipleLocator(1))
ax2.yaxis.set_major_locator(ticker.MultipleLocator(1))
plt.grid(True, which='major')



plt.tight_layout(pad=3)
plt.savefig('summary_chart_1.tif',c='k',dpi=300,format='tif')
plt.show()
