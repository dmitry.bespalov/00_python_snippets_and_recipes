﻿#python
#stats
#bernoulli


import numpy as np
from scipy import stats
import seaborn
import seaborn as sns
import matplotlib.pyplot as plt

p = 0.5
bernuilli_distribution = stats.bernoulli(p)

# probability mass function (pmf) is probability of each outcome
p_tails = bernuilli_distribution.pmf(0)
p_heads = bernuilli_distribution.pmf(1)

# Simulate trials with random variates
trials = bernuilli_distribution.rvs(10)
print(trials)