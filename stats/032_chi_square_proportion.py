import numpy as np
import scipy.stats as stats

data = np.array([[43,9],
                 [44,4]])

V, p, dof, expected = stats.chi2_contingency(data) # add correction=False for uncorrected Chi-square
print(p)
print(expected)

This tests for diffferences beyween rows which may be contigent upon columns

Note. In Chi-square at least 80% of the of the cells should have a value of at least 5,
and all cells where values are expected should be at least 1.

If this is not the case then user Fisher exact test.



