"""
Simple drawing of networkx graph

    See Also
    --------
    draw_networkx()
    draw_networkx_nodes()
    draw_networkx_edges()
    draw_networkx_labels()
    draw_networkx_edge_labels()
    
"""

import networkx as nx
import pylab as plt

def save_graph(graph,file_name,pos):
    #initialze Figure
    plt.figure(num=None, figsize=(5,5), dpi=100)
    plt.axis('off')
    fig = plt.figure(1)
    nx.draw(G, with_labels = True)
    plt.savefig(file_name,bbox_inches="tight")
    #plt.close()
    del fig

# Define network

G = nx.Graph()
G.add_edge('A', 'B', weight=4)
G.add_edge('B', 'D', weight=2)
G.add_edge('A', 'C', weight=3)
G.add_edge('C', 'D', weight=4)
G.add_edge('A', 'D', weight=5)

# Draw a simple network graph without weights 

pos = nx.spring_layout(G)
#save_graph(G,'spring_layout.png',pos)

# optionally define positions of nodes

positions = {'A':(1,10), 'B':(3,5), 'C':(1,5),'D':(10,10)}
pos = nx.spring_layout(G)
save_graph(G,'positioned_nodes.png',pos)
